package com.bookmyvahical.model;

import java.util.Map;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("upload_user_pic.php")
    Call<MyPojo> updateProfilePic(@FieldMap Map<String, String> map);
}
