package com.bookmyvahical.model;

import android.content.Context;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.util.ArrayList;
import java.util.HashMap;

public class UserData {
    private static UserData userData;
    private String Unique_Table_ID = "";
    private String UserDatehitformat = "";
    private String UserTimehitformat = "";
    private String airCondition = "";
    private HashMap<String, ArrayList<LatLng>> cabLocation = new HashMap();
    private String cab_type = "2";
    private String cardId = "";
    private String card_name = "";
    private String card_number = "";
    private boolean confirmFlag = false;
    private double dest_lat = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    private double dest_longt = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    private String desti_address = "";
    private String distance = "";
    private String distance_up_to_source = "";
    private String driverCabNo = "";
    private String driverId = "";
    private String driverLat = "";
    private String driverLong = "";
    private float driverRate = 0.0f;
    private String e_mail = "";
    private String estimatedFare = "";
    private HashMap<String, LatLng> favoritePlaces = new HashMap();
    private String futureRideDate = "";
    private String futureRideTime = "";
    private boolean hourlyFuture = false;
    private boolean hourlyInstant = false;
    private boolean laterRideFlag = false;
    private String luggage = "";
    private String multiCharger = "";
    private String music = "";
    private String name = "";
    private String office_address = "";
    private double office_lat;
    private double office_long;
    private String passengeremail = "";
    private String passengerimage = "";
    private String passengername = "";
    private String rate_type = "hour";
    private String retunDate = "";
    private boolean retunFlag = false;
    private String returnTime = "";
    private String return_driver_id = "";
    private String rideDateIns = "";
    private String rideId = "";
    private String rideTime = "";
    private String rideTimeIns = "";
    private String rideType = "";
    private double sourc_lat = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    private double sourc_longt = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    private String source_address = "";
    private String time = "";
    private String time_up_to_source = "";
    private String voucherId = "";
    private String water = "";

    public String getVoucherId() {
        return this.voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public String getUnique_Table_ID() {
        return this.Unique_Table_ID;
    }

    public void setUnique_Table_ID(String unique_Table_ID) {
        this.Unique_Table_ID = unique_Table_ID;
    }

    public String getUserTimehitformat() {
        return this.UserTimehitformat;
    }

    public void setUserTimehitformat(String userTimehitformat) {
        this.UserTimehitformat = userTimehitformat;
    }

    public String getUserDatehitformat() {
        return this.UserDatehitformat;
    }

    public void setUserDatehitformat(String userDatehitformat) {
        this.UserDatehitformat = userDatehitformat;
    }

    public static UserData getinstance(Context context) {
        if (userData == null) {
            userData = new UserData();
        }
        return userData;
    }

    public HashMap<String, LatLng> getFavoritePlaces() {
        return this.favoritePlaces;
    }

    public void setFavoritePlaces(HashMap<String, LatLng> favoritePlace) {
        this.favoritePlaces.putAll(favoritePlace);
    }

    public void removeFavoritePlaces(String favoritePlace) {
        this.favoritePlaces.remove(favoritePlace);
    }

    public String getCab_type() {
        return this.cab_type;
    }

    public void setCab_type(String cab_type) {
        this.cab_type = cab_type;
    }

    public String getPassengername() {
        return this.passengername;
    }

    public void setPassengername(String passengername) {
        this.passengername = passengername;
    }

    public String getPassengeremail() {
        return this.passengeremail;
    }

    public void setPassengeremail(String passengeremail) {
        this.passengeremail = passengeremail;
    }

    public String getPassengerimage() {
        return this.passengerimage;
    }

    public void setPassengerimage(String passengerimage) {
        this.passengerimage = passengerimage;
    }

    public String getSource_address() {
        return this.source_address;
    }

    public String getDistance() {
        return this.distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setSource_address(String source_address) {
        this.source_address = source_address;
    }

    public double getDest_lat() {
        return this.dest_lat;
    }

    public void setDest_lat(double dest_lat) {
        this.dest_lat = dest_lat;
    }

    public double getDest_longt() {
        return this.dest_longt;
    }

    public void setDest_longt(double dest_longt) {
        this.dest_longt = dest_longt;
    }

    public double getSourc_longt() {
        return this.sourc_longt;
    }

    public void setSourc_longt(double sourc_longt) {
        this.sourc_longt = sourc_longt;
    }

    public double getSourc_lat() {
        return this.sourc_lat;
    }

    public void setSourc_lat(double sourc_lat) {
        this.sourc_lat = sourc_lat;
    }

    private UserData() {
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getE_mail() {
        return this.e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public double getLat() {
        return this.dest_lat;
    }

    public void setLat(double lat) {
        this.dest_lat = lat;
    }

    public double getLongt() {
        return this.dest_longt;
    }

    public void setLongt(double longt) {
        this.dest_lat = longt;
    }

    public String getAddress() {
        return this.source_address;
    }

    public void setAddress(String address) {
        this.source_address = address;
    }

    public String getDesti_address() {
        return this.desti_address;
    }

    public void setDesti_address(String desti_address) {
        this.desti_address = desti_address;
    }

    public String getOffice_address() {
        return this.office_address;
    }

    public void setOffice_address(String office_address) {
        this.office_address = office_address;
    }

    public double getOffice_lat() {
        return this.office_lat;
    }

    public void setOffice_lat(double office_lat) {
        this.office_lat = office_lat;
    }

    public double getOffice_long() {
        return this.office_long;
    }

    public void setOffice_long(double office_long) {
        this.office_long = office_long;
    }

    public HashMap<String, ArrayList<LatLng>> getCabLocation() {
        return this.cabLocation;
    }

    public void setCabLocation(HashMap<String, ArrayList<LatLng>> cabLocation) {
        this.cabLocation = cabLocation;
    }

    public String getRetunDate() {
        return this.retunDate;
    }

    public void setRetunDate(String retunDate) {
        this.retunDate = retunDate;
    }

    public String getReturnTime() {
        return this.returnTime;
    }

    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime;
    }

    public String getFutureRideDate() {
        return this.futureRideDate;
    }

    public void setFutureRideDate(String futureRideDate) {
        this.futureRideDate = futureRideDate;
    }

    public String getFutureRideTime() {
        return this.futureRideTime;
    }

    public void setFutureRideTime(String futureRideTime) {
        this.futureRideTime = futureRideTime;
    }

    public boolean isRetunFlag() {
        return this.retunFlag;
    }

    public void setRetunFlag(boolean retunFlag) {
        this.retunFlag = retunFlag;
    }

    public boolean isLaterRideFlag() {
        return this.laterRideFlag;
    }

    public void setLaterRideFlag(boolean laterRideFlag) {
        this.laterRideFlag = laterRideFlag;
    }

    public String getReturn_driver_id() {
        return this.return_driver_id;
    }

    public void setReturn_driver_id(String return_driver_id) {
        this.return_driver_id = return_driver_id;
    }

    public String getRate_type() {
        return this.rate_type;
    }

    public void setRate_type(String rate_type) {
        this.rate_type = rate_type;
    }

    public String getRideType() {
        return this.rideType;
    }

    public void setRideType(String rideType) {
        this.rideType = rideType;
    }

    public String getRideDateIns() {
        return this.rideDateIns;
    }

    public void setRideDateIns(String rideDateIns) {
        this.rideDateIns = rideDateIns;
    }

    public String getRideTimeIns() {
        return this.rideTimeIns;
    }

    public void setRideTimeIns(String rideTimeIns) {
        this.rideTimeIns = rideTimeIns;
    }

    public String getDriverId() {
        return this.driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getRideId() {
        return this.rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getDriverLat() {
        return this.driverLat;
    }

    public void setDriverLat(String driverLat) {
        this.driverLat = driverLat;
    }

    public String getDriverLong() {
        return this.driverLong;
    }

    public void setDriverLong(String driverLong) {
        this.driverLong = driverLong;
    }

    public String getWater() {
        return this.water;
    }

    public void setWater(String water) {
        this.water = water;
    }

    public String getAirCondition() {
        return this.airCondition;
    }

    public void setAirCondition(String airCondition) {
        this.airCondition = airCondition;
    }

    public String getMusic() {
        return this.music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getMultiCharger() {
        return this.multiCharger;
    }

    public void setMultiCharger(String multiCharger) {
        this.multiCharger = multiCharger;
    }

    public String getRideTime() {
        return this.rideTime;
    }

    public void setRideTime(String rideTime) {
        this.rideTime = rideTime;
    }

    public String getLuggage() {
        return this.luggage;
    }

    public void setLuggage(String luggage) {
        this.luggage = luggage;
    }

    public String getDistance_up_to_source() {
        return this.distance_up_to_source;
    }

    public void setDistance_up_to_source(String distance_up_to_source) {
        this.distance_up_to_source = distance_up_to_source;
    }

    public String getTime_up_to_source() {
        return this.time_up_to_source;
    }

    public void setTime_up_to_source(String time_up_to_source) {
        this.time_up_to_source = time_up_to_source;
    }

    public String getEstimatedFare() {
        return this.estimatedFare;
    }

    public void setEstimatedFare(String estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    public String getCard_number() {
        return this.card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_name() {
        return this.card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public float getDriverRate() {
        return this.driverRate;
    }

    public void setDriverRate(float driverRate) {
        this.driverRate = driverRate;
    }

    public String getCardId() {
        return this.cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public boolean isConfirmFlag() {
        return this.confirmFlag;
    }

    public void setConfirmFlag(boolean confirmFlag) {
        this.confirmFlag = confirmFlag;
    }

    public boolean isHourlyInstant() {
        return this.hourlyInstant;
    }

    public void setHourlyInstant(boolean hourlyInstant) {
        this.hourlyInstant = hourlyInstant;
    }

    public boolean isHourlyFuture() {
        return this.hourlyFuture;
    }

    public void setHourlyFuture(boolean hourlyFuture) {
        this.hourlyFuture = hourlyFuture;
    }
}
