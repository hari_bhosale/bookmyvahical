package com.bookmyvahical;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bookmyvahical.helper.CheckConnectivity;
import com.bookmyvahical.utils.GPSTracker;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;

public class SplashActivity extends AppCompatActivity implements ConnectionCallbacks, OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {
    private static final int MY_PERMISSIONS_REQUEST_READ_AND_WRITE_SDK = 4;
    private final int MY_PERMISSIONS_REQUEST_GPS = 2;
    private final int MY_PERMISSIONS_REQUEST_LOCATION = 3;
    int REQUEST_CHECK_SETTINGS = 100;
    private CheckConnectivity checkConnectivity;
    private Context context;
    private GPSTracker gpsTracker;
    protected LocationRequest locationRequest;
    protected GoogleApiClient mGoogleApiClient;
    private SharedPreferencesUtility sharedPreferencesUtility;
    private ProgressBar splashProgressBar;

    /* renamed from: com.bookmyvahical.SplashActivity$1 */
    class RunThread implements Runnable {
        RunThread() {
        }

        public void run() {
            if (!SplashActivity.this.checkConnectivity.isNetworkAvailable()) {
                Toast.makeText(SplashActivity.this.context, R.string.internetdisabledmessage, Toast.LENGTH_LONG).show();
            } else if (SharedPreferencesUtility.loadUsername(SplashActivity.this.context).equalsIgnoreCase("0") && SharedPreferencesUtility.loadPassword(SplashActivity.this.context).equalsIgnoreCase("0") && !SplashActivity.this.sharedPreferencesUtility.getLoginflag(SplashActivity.this.context)) {
                SplashActivity.this.startActivity(new Intent(SplashActivity.this.context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra("issplash", true));
                SplashActivity.this.finish();
            } else {
                SplashActivity.this.startActivity(new Intent(SplashActivity.this.context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra("issplash", true));
                SplashActivity.this.finish();
            }
            SplashActivity.this.finish();
            SplashActivity.this.splashProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.activity_splash);
        this.context = this;
        try {
            this.checkConnectivity = new CheckConnectivity(this.context);
            this.sharedPreferencesUtility = SharedPreferencesUtility.getInstance();
            this.splashProgressBar = (ProgressBar) findViewById(R.id.splash_progressBar);
            this.gpsTracker = new GPSTracker(this.context);
            this.splashProgressBar.setVisibility(View.INVISIBLE);
            checkGpsPermission();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkGpsPermission() {
        if (VERSION.SDK_INT < 23 || ContextCompat.checkSelfPermission(this.context, "android.permission.ACCESS_FINE_LOCATION") == 0 || ContextCompat.checkSelfPermission(this.context, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            checkSmsPermission();
            return;
        }
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION"}, 3);
    }

    public void checkSmsPermission() {
        if (VERSION.SDK_INT < 23 || ContextCompat.checkSelfPermission(this.context, "android.permission.WRITE_EXTERNAL_STORAGE") == 0 || ContextCompat.checkSelfPermission(this.context, "android.permission.READ_EXTERNAL_STORAGE") == 0) {
            checkGpsStatus();
            return;
        }
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"}, 4);
    }

    private void checkGpsStatus() {
        try {
            if (this.gpsTracker.canGetLocation()) {
                startHandler();
                return;
            }
            this.mGoogleApiClient = new Builder(this).addApi(LocationServices.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
            this.mGoogleApiClient.connect();
            this.locationRequest = LocationRequest.create();
            this.locationRequest.setPriority(100);
            this.locationRequest.setInterval(30000);
            this.locationRequest.setFastestInterval(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onConnected(@Nullable Bundle bundle) {
        try {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(this.locationRequest);
            builder.setAlwaysShow(true);
            LocationServices.SettingsApi.checkLocationSettings(this.mGoogleApiClient, builder.build()).setResultCallback(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onConnectionSuspended(int i) {
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case 6:
                try {
                    status.startResolutionForResult(this, this.REQUEST_CHECK_SETTINGS);
                    return;
                } catch (SendIntentException e) {
                    return;
                }
            default:
                return;
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 3:
                if (grantResults.length > 0 && grantResults[0] == 0) {
                    checkSmsPermission();
                    return;
                }
                return;
            case 4:
                if (grantResults.length > 0 && grantResults[0] == 0) {
                    checkGpsStatus();
                    return;
                }
                return;
            default:
                return;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != this.REQUEST_CHECK_SETTINGS) {
            return;
        }
        if (resultCode == -1) {
            Toast.makeText(getApplicationContext(), R.string.gps_enabled, Toast.LENGTH_SHORT).show();
            startHandler();
            return;
        }
        Toast.makeText(getApplicationContext(), R.string.gps_not_enabled, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        startActivity(intent);
    }

    private void startHandler() {
        new Handler().postDelayed(new RunThread(), 3000);
    }
}
