package com.bookmyvahical;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.os.Process;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.helper.CheckConnectivity;
import com.bookmyvahical.helper.MyWebViewClient;
import com.bookmyvahical.model.UserData;
import com.bookmyvahical.services.LoginTask;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import com.google.android.gms.drive.DriveFile;

public class LoginActivity extends AppCompatActivity implements OnClickListener {
    private CheckConnectivity checkConnectivity;
    private Context context;
    private ProgressBar login_progress_bar;
    private EditText password_editext;
    private SharedPreferencesUtility sharedPreferencesUtility = SharedPreferencesUtility.getInstance();
    private UserData userdata;
    private EditText username_editext;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_login);
        this.context = this;
        try {
            this.userdata = UserData.getinstance(this.context);
            this.checkConnectivity = new CheckConnectivity(getApplicationContext());
            this.username_editext = (EditText) findViewById(R.id.username_edittext);
            this.password_editext = (EditText) findViewById(R.id.password_edittext);
            this.login_progress_bar = (ProgressBar) findViewById(R.id.loginprogress);
            this.login_progress_bar.setVisibility(4);
            findViewById(R.id.login_image_button_id).setOnClickListener(this);
            findViewById(R.id.forgot_password_textview_id).setOnClickListener(this);
            findViewById(R.id.Register_textview_id).setOnClickListener(this);
            if (getIntent().getBooleanExtra("EXIT", false)) {
                MyWebViewClient.handleIncomingIntent(this.context, SplashActivity.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void loginProcess() {
        try {
            this.login_progress_bar.setVisibility(4);
            if (!this.checkConnectivity.isNetworkAvailable()) {
                Toast.makeText(getApplicationContext(), R.string.internetdisabledmessage, Toast.LENGTH_SHORT).show();
            } else if (this.username_editext.getText().toString().length() == 0) {
                Toast.makeText(getApplicationContext(), IWebConstant.EMAIL_NOT_ENTERED, Toast.LENGTH_SHORT).show();
            } else if (!this.username_editext.getText().toString().contains("@") || !this.username_editext.getText().toString().contains(".")) {
                Toast.makeText(getApplicationContext(), IWebConstant.EMAIL_NOT_VALID, Toast.LENGTH_SHORT).show();
            } else if (this.password_editext.getText().toString().length() == 0) {
                Toast.makeText(getApplicationContext(), IWebConstant.ENTER_PASSWORD, Toast.LENGTH_SHORT).show();
            } else {
                new LoginTask(this.context, this.username_editext.getText().toString(), this.password_editext.getText().toString(), this.login_progress_bar).execute(new String[0]);
            }
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
    }

    public void onBackPressed() {
        if (this.sharedPreferencesUtility.getLoginflag(this.context)) {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            intent.setFlags(DriveFile.MODE_READ_ONLY);
            startActivity(intent);
            System.exit(0);
            Process.killProcess(Process.myPid());
            super.onDestroy();
            finish();
            return;
        }
        super.onBackPressed();
    }

    protected void onRestart() {
        super.onRestart();
        this.username_editext.setText("");
        this.password_editext.setText("");
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_image_button_id:
                loginProcess();
                return;
            case R.id.forgot_password_textview_id:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                return;
            case R.id.Register_textview_id:
                startActivity(new Intent(this, RegisterActivity.class));
                return;
            default:
                return;
        }
    }
}
