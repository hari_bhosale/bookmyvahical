package com.bookmyvahical;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.helper.MyWebViewClient;

public class RegisterActivity extends AppCompatActivity {
    Context context;
    private Handler mHandler = new Handler();
    WebView register_web_view;

    final class DemoJavaScriptInterface {

        /* renamed from: com.bookmyvahical.ccv3.RegisterActivity$DemoJavaScriptInterface$1 */
        class C03551 implements Runnable {
            C03551() {
            }

            public void run() {
                RegisterActivity.this.startActivity(new Intent(RegisterActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        }

        /* renamed from: com.bookmyvahical.ccv3.RegisterActivity$DemoJavaScriptInterface$2 */
        class C03562 implements Runnable {
            C03562() {
            }

            public void run() {
                try {
                    Toast.makeText(RegisterActivity.this.getApplicationContext(), R.string.successfully_registered, 0).show();
                    RegisterActivity.this.startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        DemoJavaScriptInterface(Context c) {
        }

        @JavascriptInterface
        public void clickOnAlreadyRegister() {
            RegisterActivity.this.mHandler.post(new C03551());
        }

        @JavascriptInterface
        public void clickOnRegister() {
            RegisterActivity.this.mHandler.post(new C03562());
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_register);
        this.context = this;
        try {
            this.register_web_view = (WebView) findViewById(R.id.register_webview);
            MyWebViewClient.enableWebViewSettings(this.register_web_view);
            this.register_web_view.setWebChromeClient(MyWebViewClient.getWebChromeInstance());
            this.register_web_view.setWebViewClient(new MyWebViewClient(this));
            this.register_web_view.addJavascriptInterface(new DemoJavaScriptInterface(this), SystemMediaRouteProvider.PACKAGE_NAME);
            this.register_web_view.loadUrl(ProjectURls.REGISTER_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
