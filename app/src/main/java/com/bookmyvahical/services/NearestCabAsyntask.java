package com.bookmyvahical.services;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.bookmyvahical.R;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.model.DriverDetails;
import com.bookmyvahical.model.UserData;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

public class NearestCabAsyntask extends AsyncTask<String, String, String> {
    private Context context;
    private GoogleMap googleMap;
    private SharedPreferencesUtility preferencesUtility;
    private String response;
    String tag = "NearestCabAsyntask";
    UserData userData;

    public NearestCabAsyntask(Context context, GoogleMap googleMap) {
        this.context = context;
        this.googleMap = googleMap;
        this.userData = UserData.getinstance(context);
    }

    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected String doInBackground(String... params) {
        String url = ProjectURls.NEAREST_CAB_URL;
        HashMap<String, String> keyValue = new HashMap();
        keyValue.put("cabtype", "" + SharedPreferencesUtility.loadCabType(this.context));
        keyValue.put("lat", "" + this.userData.getSourc_lat());
        keyValue.put("long", "" + this.userData.getSourc_longt());
        keyValue.put("email", SharedPreferencesUtility.loadUsername(this.context));
        this.response = new FetchUrl().fetchUrl(url, keyValue);
        Log.d(this.tag, this.response);
        return this.response;
    }

    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {
            JSONObject response = new JSONObject(result);
            if (response.has(IWebConstant.RESPONSE_KEY_RESPONSE)) {
                JSONObject res = response.getJSONObject(IWebConstant.RESPONSE_KEY_RESPONSE);
                String result1 = res.getString(IWebConstant.RESPONSE_KEY_RESULT);
                if (result1.equalsIgnoreCase("1")) {
                    JSONArray location = res.getJSONArray(Param.LOCATION);
                    for (int i = 0; i < result1.length(); i++) {
                        JSONObject arrayObject = location.getJSONObject(i);
                        this.googleMap.addMarker(new MarkerOptions().position(new LatLng(arrayObject.getDouble("lat"), arrayObject.getDouble("long"))).icon(BitmapDescriptorFactory.fromResource(R.drawable.chauffeurs)));
                        DriverDetails.setNoCabFound("CabFound");
                    }
                    return;
                }
                return;
            }
            DriverDetails.setNoCabFound("NoCabFound");
        } catch (Exception e) {
            Log.d(this.tag, e.toString());
        }
    }
}
