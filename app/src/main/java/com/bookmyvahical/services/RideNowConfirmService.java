package com.bookmyvahical.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.format.Time;
import android.util.Log;
import com.bookmyvahical.R;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.model.DriverDetails;
import com.bookmyvahical.model.UserData;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import java.util.HashMap;
import org.json.JSONObject;

public class RideNowConfirmService extends AsyncTask<String, String, String> {
    private String TAG = "RideNowConfirmtask";
    private Context context;
    private ProgressDialog dialog;
    private String response = "";
    private UserData userData;

    public RideNowConfirmService(Context context) {
        this.context = context;
        this.userData = UserData.getinstance(context);
    }

    protected void onPreExecute() {
        super.onPreExecute();
        this.dialog = new ProgressDialog(this.context);
        this.dialog.setMessage(this.context.getResources().getString(R.string.please_wait));
        this.dialog.show();
    }

    protected String doInBackground(String... params) {
        String url = ProjectURls.RIDE_NOW_CONFIRM_URL;
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        HashMap<String, String> keyValue = new HashMap();
        keyValue.put("email", SharedPreferencesUtility.loadUsername(this.context));
        keyValue.put("d_email", DriverDetails.getDriver_email());
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_DRIVER_CAB_TYPE, DriverDetails.getDriverCabType());
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_PICK_ADDRESS, this.userData.getAddress());
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_PICK_DATE, "" + today.year + "-" + (today.month + 1) + "-" + today.monthDay);
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_PICK_TIME, "" + today.hour + ":" + today.minute);
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_DEST_ADDRESS, this.userData.getDesti_address());
        keyValue.put(IWebConstant.Name_VALUE_PAIR_KEY_DISTANCE, this.userData.getDistance());
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_CAB_NUMBER, DriverDetails.getCabNumber());
        Log.d(this.TAG, "" + keyValue);
        try {
            this.response = new FetchUrl().fetchUrl(url, keyValue);
            Log.d(IWebConstant.RESPONSE_KEY_RESPONSE, this.response);
        } catch (Exception e) {
            e.printStackTrace();
            e.getStackTrace();
        }
        return this.response;
    }

    protected void onPostExecute(String response) {
        try {
            this.dialog.dismiss();
            this.userData.setUnique_Table_ID(new JSONObject(response).getString(IWebConstant.NAME_VALUE_PAIR_KEY_ID));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
