package com.bookmyvahical.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.model.UserData;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import java.util.HashMap;
import org.json.JSONObject;

public class ConfirmRejectCabService extends AsyncTask<String, String, String> {
    private String TAG = "confirmrejecttask";
    private Context context;
    private Context context1;
    private ProgressDialog dialog;
    int now_later_flag;
    private String response = "";
    UserData userdata;

    public ConfirmRejectCabService(Context context, int now_later_flag, Context context1) {
        this.context = context;
        this.now_later_flag = now_later_flag;
        this.context1 = context1;
        this.userdata = UserData.getinstance(context);
    }

    protected void onPreExecute() {
        super.onPreExecute();
        this.dialog = new ProgressDialog(this.context);
        this.dialog.setMessage(this.context.getResources().getString(R.string.please_wait));
        this.dialog.show();
    }

    protected String doInBackground(String... params) {
        String url;
        if (this.now_later_flag == 1) {
            url = ProjectURls.CONFIRM_REJECT_Now_URL;
        } else {
            url = ProjectURls.CONFIRM_REJECT_URL;
        }
        HashMap<String, String> keyValue = new HashMap();
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_CONFIRM_CAB, params[0]);
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_USER_TYPE, SharedPreferencesUtility.loadUserType(this.context));
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_ID, this.userdata.getUnique_Table_ID());
        Log.d(this.TAG, "" + keyValue);
        try {
            this.response = new FetchUrl().fetchUrl(url, keyValue);
            Log.d(IWebConstant.RESPONSE_KEY_RESPONSE, this.response);
        } catch (Exception e) {
            e.printStackTrace();
            e.getStackTrace();
        }
        return this.response;
    }

    protected void onPostExecute(String response) {
        try {
            this.dialog.dismiss();
            JSONObject obj = new JSONObject(response);
            String success = obj.getString("success");
            String status = obj.getString("status");
            if (success.equals("0")) {
                if (SharedPreferencesUtility.loadUserType(this.context).equalsIgnoreCase("driver")) {
                    Toast.makeText(this.context, "Cannot Confirm Booking!! Passenger has Rejected This Booking", 0).show();
                    return;
                }
                Toast.makeText(this.context, "Cannot Confirm Booking!! Driver has Rejected This Booking", 0).show();
                this.context.startActivity(new Intent(this.context1, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            } else if (status.equalsIgnoreCase(IWebConstant.CONFIRM)) {
                Toast.makeText(this.context, "Your Ride is Confirmed", 0).show();
                if (!SharedPreferencesUtility.loadUserType(this.context).equalsIgnoreCase("passenger")) {
                }
            } else {
                Toast.makeText(this.context, "Your Ride is Rejected", 0).show();
                if (SharedPreferencesUtility.loadUserType(this.context).equalsIgnoreCase("passenger")) {
                    this.context.startActivity(new Intent(this.context1, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
