package com.bookmyvahical.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.latlong.LatLongDetails;
import com.bookmyvahical.model.DriverDetails;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import java.util.HashMap;

public class Driver_loc_update_task extends AsyncTask<String, String, String> {
    private String TAG = "Gcmtask";
    private Context context;
    private ProgressDialog dialog;
    String newMessage;
    private String response = "";

    public Driver_loc_update_task(Context context, String newMessage) {
        this.context = context;
        this.newMessage = newMessage;
    }

    protected String doInBackground(String... params) {
        String url = ProjectURls.DRIVER_UPDATE_LOC_URL;
        HashMap<String, String> keyValue = new HashMap();
        keyValue.put("d_email", SharedPreferencesUtility.loadUsername(this.context));
        keyValue.put(IWebConstant.Driver_autoupdate_lat, "" + LatLongDetails.driver_latitude);
        keyValue.put(IWebConstant.Driver_autoupdate_long, "" + LatLongDetails.driver_longitude);
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_DRIVER_SEND_USER_EMAIL, this.newMessage);
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_DRIVER_STATUS, DriverDetails.getDriverStatus());
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_DRIVER_CAB_TYPE, DriverDetails.getDriverCabType());
        Log.d(this.TAG, "" + keyValue);
        try {
            this.response = new FetchUrl().fetchUrl(url, keyValue);
            Log.d(IWebConstant.RESPONSE_KEY_RESPONSE, this.response);
        } catch (Exception e) {
            e.printStackTrace();
            e.getStackTrace();
        }
        return this.response;
    }

    protected void onPostExecute(String response) {
    }
}
