package com.bookmyvahical.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.bookmyvahical.R;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import java.util.HashMap;

public class FcmTokenToServer extends AsyncTask<String, String, String> {
    private String TAG = "Gcmtask";
    private Context context;
    private ProgressDialog dialog;
    private String response = "";

    public FcmTokenToServer(Context context) {
        this.context = context;
    }

    protected void onPreExecute() {
        super.onPreExecute();
        this.dialog = new ProgressDialog(this.context);
        this.dialog.setMessage(this.context.getResources().getString(R.string.please_wait));
        this.dialog.show();
    }

    protected String doInBackground(String... params) {
        String url = ProjectURls.SERVER_URL;
        HashMap<String, String> keyValue = new HashMap();
        keyValue.put("email", SharedPreferencesUtility.loadUsername(this.context));
        keyValue.put("regId", SharedPreferencesUtility.loadRegistrationId(this.context));
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_USER_TYPE, SharedPreferencesUtility.loadUserType(this.context));
        Log.d(this.TAG, "" + keyValue);
        try {
            this.response = new FetchUrl().fetchUrl(url, keyValue);
            Log.d(IWebConstant.RESPONSE_KEY_RESPONSE, this.response);
        } catch (Exception e) {
            e.printStackTrace();
            e.getStackTrace();
        }
        return this.response;
    }

    protected void onPostExecute(String response) {
        try {
            this.dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
