package com.bookmyvahical.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.latlong.LatLongDetails;
import com.bookmyvahical.utils.GPSTracker;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class UpdateDriverLocationService extends AsyncTask<String, String, String> {
    private Context context;
    ProgressDialog dialog;
    private GPSTracker gpsTracker;
    private JSONObject json;
    private double lat;
    private Location location;
    private double longt;
    String response;
    String tag = "UpdateDriverLocationAsyntask";

    public UpdateDriverLocationService(Context context) {
        this.context = context;
    }

    protected void onPreExecute() {
        super.onPreExecute();
        try {
            this.gpsTracker = new GPSTracker(this.context);
            this.location = this.gpsTracker.getLocation();
            if (this.gpsTracker.canGetLocation()) {
                this.lat = this.location.getLatitude();
            }
            this.longt = this.location.getLongitude();
            LatLongDetails.driver_latitude = this.lat;
            LatLongDetails.driver_longitude = this.longt;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected String doInBackground(String... params) {
        String url = ProjectURls.GET_DRIVER_CURRENT_LOCATION;
        HashMap<String, String> keyValue = new HashMap();
        keyValue.put("driver_id", "" + params[0]);
        keyValue.put("lat", "" + LatLongDetails.driver_latitude);
        keyValue.put("long", "" + LatLongDetails.driver_longitude);
        this.response = new FetchUrl().fetchUrl(url, keyValue);
        Log.d(this.tag, this.response);
        return this.response;
    }

    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {
            JSONObject jSONObject = new JSONObject(result);
        } catch (JSONException e) {
            Log.e("EXCEPTION", "Parsing update_driver location result");
            e.printStackTrace();
        }
    }
}
