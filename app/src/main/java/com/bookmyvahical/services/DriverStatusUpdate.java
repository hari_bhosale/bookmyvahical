package com.bookmyvahical.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.bookmyvahical.R;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.model.DriverDetails;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import java.util.HashMap;
import org.json.JSONObject;

public class DriverStatusUpdate extends AsyncTask<String, String, String> {
    private String TAG = "driverstatustask";
    private Context context;
    private ProgressDialog dialog;
    private String response = "";

    public DriverStatusUpdate(Context context) {
        this.context = context;
    }

    protected void onPreExecute() {
        super.onPreExecute();
        this.dialog = new ProgressDialog(this.context);
        this.dialog.setMessage(this.context.getResources().getString(R.string.updating_status));
        this.dialog.show();
    }

    protected String doInBackground(String... params) {
        String url = ProjectURls.DRIVER_STATUS_CHANGE_URL;
        HashMap<String, String> keyValue = new HashMap();
        keyValue.put("email", SharedPreferencesUtility.loadUsername(this.context));
        keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_DRIVER_STATUS, DriverDetails.getDriverStatus().toLowerCase());
        Log.d(this.TAG, "" + keyValue);
        try {
            this.response = new FetchUrl().fetchUrl(url, keyValue);
            Log.d(IWebConstant.RESPONSE_KEY_RESPONSE, this.response);
        } catch (Exception e) {
            e.printStackTrace();
            e.getStackTrace();
        }
        return this.response;
    }

    protected void onPostExecute(String response) {
        try {
            this.dialog.dismiss();
            int result = new JSONObject(response).getInt("success");
            if (result == 1) {
                Toast.makeText(this.context, this.context.getString(R.string.status_updated) + DriverDetails.getDriverStatus().toUpperCase().charAt(0) + DriverDetails.getDriverStatus().substring(1), 0).show();
            }
            if (result != 0) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
