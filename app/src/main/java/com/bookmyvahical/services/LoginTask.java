package com.bookmyvahical.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import java.util.HashMap;
import org.json.JSONObject;

public class LoginTask extends AsyncTask<String, String, String> {
    private String TAG = "Logintask";
    private Context context;
    private ProgressDialog dialog;
    private ProgressBar login_progress_bar;
    private String password;
    private String response = "";
    private SharedPreferencesUtility sharedPreferencesUtility = SharedPreferencesUtility.getInstance();
    private String userName;

    public LoginTask(Context context, String userName, String password, ProgressBar login_progress_bar) {
        this.context = context;
        this.userName = userName;
        this.password = password;
        this.login_progress_bar = login_progress_bar;
    }

    protected void onPreExecute() {
        super.onPreExecute();
        this.dialog = new ProgressDialog(this.context);
        this.dialog.setMessage(this.context.getResources().getString(R.string.please_wait));
        this.dialog.show();
    }

    protected String doInBackground(String... params) {
        String url = ProjectURls.LOGIN_URL_STRING;
        HashMap<String, String> keyValue = new HashMap();
        keyValue.put("username", this.userName);
        keyValue.put("password", this.password);
        Log.d(this.TAG, "" + keyValue);
        try {
            this.response = new FetchUrl().fetchUrl(url, keyValue);
            Log.d(IWebConstant.RESPONSE_KEY_RESPONSE, this.response);
        } catch (Exception e) {
            e.printStackTrace();
            e.getStackTrace();
        }
        return this.response;
    }

    protected void onPostExecute(String response) {
        try {
            this.dialog.dismiss();
            JSONObject obj = new JSONObject(response);
            int result = obj.getInt("success");
            if (result == 1) {
                String userType = obj.getString(IWebConstant.NAME_VALUE_PAIR_USER_TYPE);
                SharedPreferencesUtility.saveUsername(this.context, this.userName);
                SharedPreferencesUtility.savePassword(this.context, this.password);
                this.sharedPreferencesUtility.setLoginflag(this.context, true);
                SharedPreferencesUtility.saveUserType(this.context, userType);
                SharedPreferencesUtility.saveProfilePic(this.context, "");
                if (userType.equalsIgnoreCase("passenger")) {
                    Intent i = new Intent(this.context, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    this.context.startActivity(i);
                } else if (userType.equalsIgnoreCase("driver")) {
                    Intent main_to_driver_intent = new Intent(this.context, MainActivity.class);
                    main_to_driver_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    this.context.startActivity(main_to_driver_intent);
                } else {
                    Toast.makeText(this.context, "Usertype not found", 0).show();
                }
            }
            if (result == 0) {
                SharedPreferencesUtility.saveUsername(this.context, "0");
                this.sharedPreferencesUtility.setLoginflag(this.context, false);
                SharedPreferencesUtility.savePassword(this.context, "0");
                Toast.makeText(this.context, "Wrong user name or password", 0).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
