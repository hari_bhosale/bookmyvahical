package com.bookmyvahical.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.widget.Toast;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.constants.ProjectURls;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class GetDriverLocationService extends AsyncTask<String, String, String> {
    Context context;
    private ProgressDialog dialog;
    private GoogleMap googleMap;
    private MarkerOptions marker;
    String name;
    HashMap<String, String> values = new HashMap();

    public GetDriverLocationService(Context context, GoogleMap googleMap) {
        this.context = context;
        this.googleMap = googleMap;
    }

    protected void onPreExecute() {
        this.dialog = ProgressDialog.show(this.context, null, "Loading ...", true, false);
    }

    protected String doInBackground(String... params) {
        this.name = new FetchUrl().fetchUrl(ProjectURls.TRACK_DRIVER_URL + params[0], this.values);
        return this.name;
    }

    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        this.dialog.dismiss();
        try {
            JSONObject responce = new JSONObject(result).getJSONObject(IWebConstant.RESPONSE_KEY_RESPONSE);
            String resultStr = responce.getString(IWebConstant.RESPONSE_KEY_RESULT);
            if (resultStr.equalsIgnoreCase("1")) {
                double lat = Double.parseDouble(responce.getString("lat"));
                double longt = Double.parseDouble(responce.getString("long"));
                this.googleMap.addMarker(new MarkerOptions().position(new LatLng(lat, longt)).title(getAddress("" + lat, "" + longt).replaceAll("\n", "")).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))).showInfoWindow();
                this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longt), 12.0f));
            } else if (resultStr.equalsIgnoreCase("3")) {
                this.context.startActivity(new Intent(this.context, MainActivity.class));
                Toast.makeText(this.context, "Unable to get location", 1).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getAddress(String lat, String lon) {
        String ret = "";
        try {
            List<Address> addresses = new Geocoder(this.context).getFromLocation(Double.parseDouble(lat), Double.parseDouble(lon), 1);
            if (addresses == null) {
                return "No Address returned!";
            }
            Address returnedAddress = (Address) addresses.get(0);
            StringBuilder strReturnedAddress = new StringBuilder("");
            for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
            }
            return strReturnedAddress.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "Can't get Address!";
        }
    }
}
