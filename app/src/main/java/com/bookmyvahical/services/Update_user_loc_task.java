package com.bookmyvahical.services;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.LinearLayout;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.fragments.RideFragment;
import com.bookmyvahical.model.DriverDetails;
import com.bookmyvahical.model.UserData;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import java.util.HashMap;
import org.json.JSONObject;

public class Update_user_loc_task extends AsyncTask<String, String, String> {
    private String TAG = "updateloctask";
    private Context context;
    private String response = "";
    private RideFragment rideFragment;
    private LinearLayout ridenowlaterlayout;
    UserData userdata;

    public Update_user_loc_task(Context context, LinearLayout ridenowlaterlayout, RideFragment rideFragment) {
        this.context = context;
        this.rideFragment = rideFragment;
        this.ridenowlaterlayout = ridenowlaterlayout;
        this.userdata = UserData.getinstance(context);
    }

    protected void onPreExecute() {
        super.onPreExecute();
        this.ridenowlaterlayout.setVisibility(4);
    }

    protected String doInBackground(String... params) {
        String url = ProjectURls.UPDATE_USER_LOCATION_URL;
        HashMap<String, String> keyValue = new HashMap();
        keyValue.put("email", SharedPreferencesUtility.loadUsername(this.context));
        keyValue.put("lat", "" + this.userdata.getSourc_lat());
        keyValue.put("long", "" + this.userdata.getSourc_longt());
        keyValue.put("cabtype", this.userdata.getCab_type());
        keyValue.put(IWebConstant.Name_VALUE_PAIR_KEY_DISTANCE, this.userdata.getDistance());
        Log.d(this.TAG, "" + keyValue);
        try {
            this.response = new FetchUrl().fetchUrl(url, keyValue);
            JSONObject jsonresult = new JSONObject(this.response);
            DriverDetails.setCabNumber("NOT FOUND");
            DriverDetails.setDriverName("NOT FOUND");
            DriverDetails.setDriverNumber("NOT FOUND");
            DriverDetails.setNearesCabReachingTime("NOT FOUND");
            DriverDetails.setNearestCabDistance((int) Float.parseFloat(jsonresult.getString(IWebConstant.Name_VALUE_PAIR_KEY_DISTANCE)));
            int time = jsonresult.getInt("reach_time");
            int hour = time / 60;
            int min = time % 60;
            if (hour == 0) {
                DriverDetails.setNearesCabReachingTime("" + min + " min");
            } else {
                DriverDetails.setNearesCabReachingTime("" + hour + " h " + min + " min");
            }
            DriverDetails.setNearesCabReachingTime("" + jsonresult.getInt("reach_time") + " min");
            DriverDetails.setCabNumber(jsonresult.getString(IWebConstant.NAME_VALUE_PAIR_KEY_CAB_NUMBER));
            DriverDetails.setDriverName(jsonresult.getString(IWebConstant.REQUEST_PARAMETER_KEY_NAME));
            DriverDetails.setDriverNumber(jsonresult.getString("number"));
            DriverDetails.setDriver_email(jsonresult.getString("email"));
            DriverDetails.setFare(jsonresult.getString("fare"));
            DriverDetails.setFarePerUnit(jsonresult.getString("fare_per_unit"));
            Log.d(IWebConstant.RESPONSE_KEY_RESPONSE, this.response);
        } catch (Exception e) {
            e.printStackTrace();
            e.getStackTrace();
        }
        return this.response;
    }

    protected void onPostExecute(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            if (!(DriverDetails.getDriverNumber().equals("NOT FOUND") && DriverDetails.getCabNumber().equals("NOT FOUND")) && (DriverDetails.getDriverNumber().length() > 1 || DriverDetails.getCabNumber().length() > 1)) {
                this.rideFragment.responseSecond();
            } else {
                this.rideFragment.responseFirst();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
