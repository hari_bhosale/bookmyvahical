package com.bookmyvahical.services;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.webkit.WebView;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.constants.ProjectURls;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class UpdatePaymentDetails extends AsyncTask<String, String, String> {
    Context context;
    JSONObject js;
    String payment_id;
    private String response = "";
    WebView rides_webview;
    String table_id;
    String tag = "PaymentDetailsAsyntask";

    public UpdatePaymentDetails(Context context, WebView rides_webview, String payment_id) {
        this.context = context;
        this.payment_id = payment_id;
        this.rides_webview = rides_webview;
    }

    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected String doInBackground(String... params) {
        try {
            String url = ProjectURls.Update_Payment_Details_URL;
            HashMap<String, String> keyValue = new HashMap();
            keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_PAYMENT_ID, this.payment_id);
            keyValue.put(IWebConstant.Name_VALUE_PAIR_KEY_TRANS_ID, "7890");
            keyValue.put(IWebConstant.NAME_VALUE_PAIR_KEY_TRANS_STATE, "demosuccess");
            this.response = new FetchUrl().fetchUrl(url, keyValue);
            Log.d(this.tag, this.response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.response;
    }

    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {
            if (new JSONObject(result).has("success") && new JSONObject(result).getString("success").equals("1")) {
                this.rides_webview.loadUrl(ProjectURls.PAYMENT_SUCCESS_URL);
            } else {
                this.rides_webview.loadUrl(ProjectURls.PAYMENT_FAIL_URL);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
