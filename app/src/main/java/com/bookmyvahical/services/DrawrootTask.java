package com.bookmyvahical.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageButton;
import com.bookmyvahical.R;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.model.DriverDetails;
import com.bookmyvahical.model.UserData;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class DrawrootTask extends AsyncTask<String, String, String> {
    public static double dist;
    public static boolean flagCompleted = false;
    public static double time;
    private ImageButton bookNowButton;
    private Context context;
    private double dest_lat = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    private double dest_long = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    String distanceText = "";
    String durationText = "";
    private GoogleMap googleMap;
    private Polyline line;
    private ProgressDialog progressDialog;
    private double source_lat = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    private double source_long = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    String tag = "DrawRootTask";
    UserData userdata;

    public DrawrootTask(Context context, LatLng source, LatLng destination, GoogleMap googleMap, ImageButton bookNowButton) {
        this.source_lat = source.latitude;
        this.source_long = source.longitude;
        this.dest_lat = destination.latitude;
        this.dest_long = destination.longitude;
        this.googleMap = googleMap;
        this.context = context;
        this.bookNowButton = bookNowButton;
        this.userdata = UserData.getinstance(context);
    }

    protected void onPreExecute() {
        super.onPreExecute();
        this.progressDialog = new ProgressDialog(this.context);
        this.progressDialog.setMessage(this.context.getResources().getString(R.string.drawing_route));
        this.progressDialog.setIndeterminate(true);
        this.progressDialog.setCancelable(false);
        this.progressDialog.show();
    }

    protected String doInBackground(String... params) {
        String json = "";
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        HashMap<String, String> keyValue = new HashMap();
        urlString.append("?origin=");
        urlString.append(Double.toString(this.source_lat));
        urlString.append(",");
        urlString.append(Double.toString(this.source_long));
        urlString.append("&destination=");
        urlString.append(Double.toString(this.dest_lat));
        urlString.append(",");
        urlString.append(Double.toString(this.dest_long));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        json = new FetchUrl().fetchUrl(urlString.toString(), keyValue);
        Log.e("Buffer Error", json);
        return json;
    }

    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {
            this.progressDialog.dismiss();
            this.bookNowButton.setVisibility(0);
            JSONObject routes = new JSONObject(result).getJSONArray("routes").getJSONObject(0);
            List<LatLng> list = decodePoly(routes.getJSONObject("overview_polyline").getString("points"));
            for (int z = 0; z < list.size() - 1; z++) {
                LatLng src = (LatLng) list.get(z);
                LatLng dest = (LatLng) list.get(z + 1);
                GoogleMap googleMap = this.googleMap;
                PolylineOptions polylineOptions = new PolylineOptions();
                r22 = new LatLng[2];
                r22[0] = new LatLng(src.latitude, src.longitude);
                r22[1] = new LatLng(dest.latitude, dest.longitude);
                this.line = googleMap.addPolyline(polylineOptions.add(r22).width(3.0f).color(this.context.getResources().getColor(R.color.colorDrawRouteLine)).geodesic(true));
                Log.i("draw root", "" + this.line.toString());
            }
            JSONObject steps = routes.getJSONArray("legs").getJSONObject(0);
            JSONObject duration = steps.getJSONObject("duration");
            JSONObject distance = steps.getJSONObject(IWebConstant.Name_VALUE_PAIR_KEY_DISTANCE);
            this.distanceText = distance.getString("text");
            this.durationText = duration.getString("text");
            Log.i("draw root", "" + distance.toString());
            dist = Double.parseDouble(distance.getString("text").replaceAll("[^\\.0123456789]", ""));
            time = Double.parseDouble(duration.getString("text").replaceAll("[^\\.0123456789]", ""));
            this.userdata.setDistance(this.distanceText);
            this.userdata.setTime(this.durationText);
            DriverDetails.setTravelTime(this.durationText);
            Log.d(this.tag, "distace is " + this.distanceText + " time is " + this.durationText);
            flagCompleted = true;
        } catch (JSONException e) {
            Log.d("draw root", "" + e);
        }
    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList();
        int index = 0;
        int len = encoded.length();
        int lat = 0;
        int lng = 0;
        while (index < len) {
            int index2;
            int shift = 0;
            int result = 0;
            while (true) {
                index2 = index + 1;
                int b = encoded.charAt(index) - 63;
                result |= (b & 31) << shift;
                shift += 5;
                if (b < 32) {
                    break;
                }
                index = index2;
            }
            lat += (result & 1) != 0 ? (result >> 1) ^ -1 : result >> 1;
            shift = 0;
            result = 0;
            index = index2;
            while (true) {
                index2 = index + 1;
                b = encoded.charAt(index) - 63;
                result |= (b & 31) << shift;
                shift += 5;
                if (b < 32) {
                    break;
                }
                index = index2;
            }
            lng += (result & 1) != 0 ? (result >> 1) ^ -1 : result >> 1;
            poly.add(new LatLng(((double) lat) / 100000.0d, ((double) lng) / 100000.0d));
            index = index2;
        }
        return poly;
    }
}
