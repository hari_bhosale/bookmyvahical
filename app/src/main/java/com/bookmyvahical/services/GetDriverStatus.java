package com.bookmyvahical.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bookmyvahical.R;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.model.DriverDetails;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import java.util.HashMap;
import org.json.JSONObject;

public class GetDriverStatus extends AsyncTask<String, String, String> {
    private String TAG = "getdriverstatustask";
    private Context context;
    private ProgressDialog dialog;
    private DriverDetails driverDetails;
    private ImageView driverStatusimageview;
    private TextView driverStatustextview;
    private String response = "";
    private LinearLayout statuslinearlayout;

    public GetDriverStatus(Context context, ImageView driverStatusimageview, TextView driverStatustextview, LinearLayout statuslinearlayout) {
        this.context = context;
        this.driverStatusimageview = driverStatusimageview;
        this.driverStatustextview = driverStatustextview;
        this.statuslinearlayout = statuslinearlayout;
    }

    protected void onPreExecute() {
        super.onPreExecute();
        this.statuslinearlayout.setVisibility(4);
        this.dialog = ProgressDialog.show(this.context, null, this.context.getString(R.string.get_d_status_dialog), true, false);
    }

    protected String doInBackground(String... params) {
        String url = ProjectURls.GET_DRIVER_STATUS_URL;
        HashMap<String, String> keyValue = new HashMap();
        keyValue.put("d_email", SharedPreferencesUtility.loadUsername(this.context));
        Log.d(this.TAG, "" + keyValue);
        try {
            this.response = new FetchUrl().fetchUrl(url, keyValue);
            Log.d(IWebConstant.RESPONSE_KEY_RESPONSE, this.response);
        } catch (Exception e) {
            e.printStackTrace();
            e.getStackTrace();
        }
        return this.response;
    }

    protected void onPostExecute(String response) {
        try {
            this.dialog.dismiss();
            JSONObject obj = new JSONObject(response);
            String status = obj.getString(IWebConstant.NAME_VALUE_PAIR_KEY_DRIVER_STATUS);
            String cabType = obj.getString(IWebConstant.NAME_VALUE_PAIR_KEY_CAB_TYPE);
            if (cabType.equalsIgnoreCase("7")) {
                DriverDetails.setDriverCabType("1");
            } else if (cabType.equalsIgnoreCase("8")) {
                DriverDetails.setDriverCabType("2");
            } else {
                DriverDetails.setDriverCabType("3");
            }
            if (status.contains("available")) {
                this.statuslinearlayout.setVisibility(0);
                this.driverStatusimageview.setImageResource(R.drawable.available);
                this.driverStatustextview.setText("Available");
                DriverDetails.setDriverStatus("available");
            } else if (status.contains("pending")) {
                this.statuslinearlayout.setVisibility(0);
                this.driverStatusimageview.setImageResource(R.drawable.pending);
                this.driverStatustextview.setText("Pending");
                DriverDetails.setDriverStatus("pending");
            } else if (status.contains("booked")) {
                this.statuslinearlayout.setVisibility(0);
                this.driverStatusimageview.setImageResource(R.drawable.booked);
                this.driverStatustextview.setText("Booked");
                DriverDetails.setDriverStatus("booked");
            } else {
                this.statuslinearlayout.setVisibility(0);
                this.driverStatusimageview.setImageResource(R.drawable.available);
                this.driverStatustextview.setText("No Status Available !");
                DriverDetails.setDriverStatus("no status available");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
