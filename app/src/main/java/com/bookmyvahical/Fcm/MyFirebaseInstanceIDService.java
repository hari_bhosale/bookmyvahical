package com.bookmyvahical.Fcm;

import android.content.Context;
import android.util.Log;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";
    Context context;

    public void onTokenRefresh() {
        this.context = getApplicationContext();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        SharedPreferencesUtility.saveRegistrationId(this.context, refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
    }
}
