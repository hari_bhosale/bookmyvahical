package com.bookmyvahical.Fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.services.Driver_loc_update_task;
import com.bookmyvahical.utils.WakeLocker;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    String message1;

    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Map<String, String> message = remoteMessage.getData();
        Log.d(TAG, "Notification Message Body: " + message);
        NotificationFilter.processMyNoti(getApplicationContext(), message);
        if (message.containsKey("noti_data")) {
            this.message1 = (String) message.get("noti_data");
            try {
                WakeLocker.acquire(getApplicationContext());
                update_driverlocation(getApplicationContext(), this.message1);
                WakeLocker.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void update_driverlocation(Context context, String newMessage) {
        Driver_loc_update_task task = new Driver_loc_update_task(context, newMessage);
        if (newMessage.contains("@")) {
            task.execute(new String[0]);
        }
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        ((NotificationManager) getSystemService("notification")).notify(0, new Builder(this).setSmallIcon(R.mipmap.notificationicon).setContentTitle("Cabbooking Notification").setContentText(messageBody).setAutoCancel(true).setSound(RingtoneManager.getDefaultUri(2)).setContentIntent(PendingIntent.getActivity(this, 0, intent, 1073741824)).build());
    }
}
