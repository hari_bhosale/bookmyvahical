package com.bookmyvahical.Fcm;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public final class CommonUtilities {
    public static final String DISPLAY_MESSAGE_ACTION = "com.bookmyvahical.DISPLAY_MESSAGE";
    public static final String EXTRA_MESSAGE = "message";
    static final String TAG = "FCM";

    public static void displayMessage(Context context, String message) {
        Log.e("DISPLAY MESSAGE", "   " + message);
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra("message", message);
        context.sendBroadcast(intent);
    }
}
