package com.bookmyvahical.Fcm;

import android.content.Context;
import com.bookmyvahical.services.UpdateDriverLocationService;
import java.util.Map;

public class NotificationFilter {
    static String flag = "";
    private static String message;
    static String message1;
    private static String[] msg = new String[3];

    public static void processMyNoti(Context context, Map<String, String> message) {
        flag = "";
        if (message.containsKey("noti_data")) {
            message1 = (String) message.get("noti_data");
            flag = "1";
        }
        if (message.containsKey("ride_confirm_msg")) {
            message1 = (String) message.get("ride_confirm_msg");
            flag = "2";
        }
        if (message.containsKey("ride_reject_msg")) {
            message1 = (String) message.get("ride_reject_msg");
            flag = "3";
        }
        if (message.containsKey("ride_now_confirm_msg")) {
            message1 = (String) message.get("ride_now_confirm_msg");
            flag = "4";
        }
        if (message.containsKey("ride_now_reject_msg")) {
            message1 = (String) message.get("ride_now_reject_msg");
            flag = "5";
        }
        if (message.containsKey("passenger_payment_msg")) {
            message1 = (String) message.get("passenger_payment_msg");
            flag = "6";
        }
        if (message.containsKey("driver_payment_msg")) {
            message1 = (String) message.get("driver_payment_msg");
            flag = "7";
        }
        if (message.containsKey("inform_driver_msg")) {
            message1 = (String) message.get("inform_driver_msg");
            flag = "8";
        }
        if (message.containsKey("inform_passenger_msg")) {
            message1 = (String) message.get("inform_passenger_msg");
            flag = "9";
        }
        if (message.containsKey("update_key")) {
            message1 = (String) message.get("update_key");
            msg = ((String) message.get("update_key")).split("'");
            new UpdateDriverLocationService(context).execute(new String[]{msg[2]});
        }
        if (message.containsKey("payment")) {
            message1 = (String) message.get("payment_method");
            flag = "a";
        }
        CommonUtilities.displayMessage(context, flag + message1);
    }
}
