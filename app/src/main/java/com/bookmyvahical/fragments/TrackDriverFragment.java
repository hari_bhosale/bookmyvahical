package com.bookmyvahical.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bookmyvahical.R;
import com.bookmyvahical.services.GetDriverLocationService;
import com.bookmyvahical.utils.GPSTracker;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MarkerOptions;

public class TrackDriverFragment extends BaseFragment {
    private static GoogleMap googlemap;
    Bundle bundle;
    private Context context;
    private ProgressDialog dialog;
    String driverId = "";
    private GPSTracker gpsTracker;
    private MarkerOptions marker;
    private SharedPreferences preferences;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_track_driver, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle((int) R.string.item_track_driver);
        this.context = getActivity();
        try {
            this.bundle = getArguments();
            if (this.bundle != null) {
                this.driverId = this.bundle.getString("driver_id");
            }
            if (googlemap == null) {
                googlemap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapfragment_trackdriver)).getMap();
                if (googlemap != null) {
                    new GetDriverLocationService(this.context, googlemap).execute(new String[]{this.driverId});
                }
            } else {
                googlemap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapfragment_trackdriver)).getMap();
                new GetDriverLocationService(this.context, googlemap).execute(new String[]{this.driverId});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    protected void backPressed() {
        replaceFragment(this.context, new RideDetailsFragment());
    }

    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager fm = getChildFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.mapfragment_trackdriver);
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fragment);
        ft.commitAllowingStateLoss();
    }
}
