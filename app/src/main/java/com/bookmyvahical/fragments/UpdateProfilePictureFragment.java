package com.bookmyvahical.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.model.ApiClient;
import com.bookmyvahical.model.ApiInterface;
import com.bookmyvahical.model.MyPojo;
import com.bookmyvahical.model.UserData;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfilePictureFragment extends BaseFragment implements OnClickListener {
    private static final int SELECT_REQUEST = 1;
    private static final int TAKE_REQUEST = 1888;
    private final int MY_PERMISSIONS_REQUEST_LOCATION = 5;
    String base64String;
    private Context context;
    private ImageView pic_imageview;
    private ImageButton selectPicture;
    private ImageButton takePicture;
    private ImageButton updateToServer;
    private UserData userdata;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_update_profile_picture, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle((int) R.string.item_update_profilepic);
        this.context = getActivity();
        try {
            this.pic_imageview = (ImageView) root.findViewById(R.id.update_profile_preview);
            this.updateToServer = (ImageButton) root.findViewById(R.id.update_to_Server);
            this.takePicture = (ImageButton) root.findViewById(R.id.take_profile_picture);
            this.selectPicture = (ImageButton) root.findViewById(R.id.select_profile_pic);
            this.updateToServer.setOnClickListener(this);
            this.takePicture.setOnClickListener(this);
            this.selectPicture.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    protected void backPressed() {
        try {
            replaceFragment(this.context, new ProfileFragment());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.take_profile_picture:
                if (ContextCompat.checkSelfPermission(this.context, "android.permission.CAMERA") != 0) {
                    requestPermissions(new String[]{"android.permission.CAMERA"}, 5);
                    return;
                }
                startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), TAKE_REQUEST);
                return;
            case R.id.select_profile_pic:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction("android.intent.action.GET_CONTENT");
                intent.addCategory("android.intent.category.OPENABLE");
                startActivityForResult(intent, 1);
                return;
            case R.id.update_to_Server:
                if (this.pic_imageview.getDrawable() == null) {
                    Toast.makeText(this.context, R.string.image_not_selected_msg, 0);
                    return;
                }
                byte[] ba;
                Bitmap bitmap = ((BitmapDrawable) this.pic_imageview.getDrawable()).getBitmap();
                do {
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    Log.e("BEFORE REDUCING", bitmap.getHeight() + " " + bitmap.getWidth() + " " + (bitmap.getRowBytes() * bitmap.getHeight()));
                    Log.e("After REDUCING", bitmap.getHeight() + " " + bitmap.getWidth() + " " + (bitmap.getRowBytes() * bitmap.getHeight()));
                    bitmap.compress(CompressFormat.JPEG, 90, bao);
                    ba = bao.toByteArray();
                    if (ba.length / 1024 >= 650) {
                        bitmap = Bitmap.createScaledBitmap(bitmap, (int) (((double) bitmap.getWidth()) * 0.95d), (int) (((double) bitmap.getHeight()) * 0.95d), true);
                    }
                    Log.e("BYTE LENGTH", "" + (ba.length / 1024));
                } while (ba.length / 1024 >= 650);
                this.base64String = Base64.encodeToString(ba, 0);
                UpdateProfilePicture(this.base64String);
                return;
            default:
                return;
        }
    }

    public void UpdateProfilePicture(final String base64String) {
        ApiInterface api = (ApiInterface) ApiClient.getClient().create(ApiInterface.class);
        HashMap<String, String> hashMap = new HashMap();
        hashMap.put(IWebConstant.NAME_VALUE_PAIR_KEY_UPDATEPIC, base64String);
        hashMap.put("email", SharedPreferencesUtility.loadUsername(this.context));
        api.updateProfilePic(hashMap).enqueue(new Callback<MyPojo>() {
            public void onResponse(Call<MyPojo> call, Response<MyPojo> response) {
                try {
                    if (((MyPojo) response.body()).getSuccess().equalsIgnoreCase("1")) {
                        SharedPreferencesUtility.saveProfilePic(UpdateProfilePictureFragment.this.context, base64String);
                        ((MainActivity) UpdateProfilePictureFragment.this.context).getSupportFragmentManager().beginTransaction().replace(R.id.containerView, new ProfileFragment()).commit();
                        return;
                    }
                    Toast.makeText(UpdateProfilePictureFragment.this.context, R.string.profile_updated_fail_msg, 1).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(Call<MyPojo> call, Throwable t) {
            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5 && grantResults[0] == 0) {
            startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), TAKE_REQUEST);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getActivity();
        if (resultCode == -1) {
            this.updateToServer.setVisibility(0);
            if (requestCode == TAKE_REQUEST) {
                this.pic_imageview.setImageBitmap((Bitmap) data.getExtras().get(IWebConstant.REQUEST_PARAMETER_KEY_DATA));
            } else if (requestCode == 1) {
                try {
                    InputStream stream = getActivity().getContentResolver().openInputStream(data.getData());
                    Bitmap pic = BitmapFactory.decodeStream(stream);
                    stream.close();
                    this.pic_imageview.setImageBitmap(pic);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(this.context, R.string.updating_status, 1).show();
                } catch (IOException e2) {
                    e2.printStackTrace();
                    Toast.makeText(this.context, R.string.updating_status, 1).show();
                }
            }
        }
    }
}
