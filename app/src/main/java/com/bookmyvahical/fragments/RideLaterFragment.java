package com.bookmyvahical.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.bookmyvahical.R;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.helper.CheckConnectivity;
import com.bookmyvahical.helper.CheckDateNTime;
import com.bookmyvahical.login.LoginDetails;
import com.bookmyvahical.model.DriverDetails;
import com.bookmyvahical.model.UserData;
import com.bookmyvahical.services.ConfirmRejectCabService;
import com.bookmyvahical.services.RideLaterService;

public class RideLaterFragment extends BaseFragment implements OnClickListener {
    LinearLayout book_button_layout;
    ImageButton booknow_button;
    TextView cab_number;
    TextView cab_type;
    private CheckConnectivity checkConnectivity;
    LinearLayout confirm_button_layout;
    ImageButton confirmnow_button;
    private Context context;
    TextView currentloc;
    TextView date;
    TextView destinationloc;
    TextView distance;
    private DriverDetails driverDetails;
    TextView driver_name;
    TextView driver_number;
    TextView fare;
    LinearLayout fareRideLaterLayout;
    TextView fare_approx;
    private String phoneNumber;
    ImageButton reject_button;
    TextView time;
    private UserData userData;

    /* renamed from: com.bookmyvahical.fragments.RideLaterFragment$2 */
    class C03752 implements DialogInterface.OnClickListener {
        C03752() {
        }

        public void onClick(DialogInterface dialog, int id) {
            RideLaterFragment.this.book_button_layout.setVisibility(8);
            RideLaterFragment.this.confirm_button_layout.setVisibility(0);
            dialog.cancel();
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_ride_later, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle((int) R.string.item_ride_later);
        try {
            this.context = getActivity();
            this.userData = UserData.getinstance(this.context);
            this.driverDetails = DriverDetails.getinstance(this.context);
            this.date = (TextView) root.findViewById(R.id.pick_up_date_RL);
            this.time = (TextView) root.findViewById(R.id.pickup_time_RL);
            this.currentloc = (TextView) root.findViewById(R.id.current_loc_RideLater);
            this.destinationloc = (TextView) root.findViewById(R.id.destination_RideLater);
            this.cab_type = (TextView) root.findViewById(R.id.cab_type_RideLater);
            this.fare = (TextView) root.findViewById(R.id.fare_RideLater);
            this.fare_approx = (TextView) root.findViewById(R.id.fare_approx_RL);
            this.distance = (TextView) root.findViewById(R.id.distance_RideLater);
            this.driver_name = (TextView) root.findViewById(R.id.driver_name_RideLater);
            this.driver_number = (TextView) root.findViewById(R.id.driver_contact_num_RideLater);
            this.cab_number = (TextView) root.findViewById(R.id.Cab_num_RideLater);
            this.book_button_layout = (LinearLayout) root.findViewById(R.id.booknow_button_layout);
            this.fareRideLaterLayout = (LinearLayout) root.findViewById(R.id.fare_distance_layout_RL);
            this.confirm_button_layout = (LinearLayout) root.findViewById(R.id.confim_button_layout);
            this.reject_button = (ImageButton) root.findViewById(R.id.reject_img_user_rl);
            this.booknow_button = (ImageButton) root.findViewById(R.id.booknowbutton_RL);
            this.confirmnow_button = (ImageButton) root.findViewById(R.id.confirm_img_user_rl);
            this.checkConnectivity = new CheckConnectivity(this.context);
            this.reject_button.setOnClickListener(this);
            this.booknow_button.setOnClickListener(this);
            this.confirmnow_button.setOnClickListener(this);
            this.date.setOnClickListener(this);
            this.time.setOnClickListener(this);
            this.driver_number.setOnClickListener(this);
            setData();
            selectDateAndTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    private void selectDateAndTime() {
        final View dialogView = View.inflate(this.context, R.layout.datetimepicker, null);
        final AlertDialog alertDialog = new Builder(this.context).create();
        alertDialog.setView(dialogView);
        alertDialog.show();
        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new OnClickListener() {
            @SuppressLint({"NewApi"})
            public void onClick(View view) {
                DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
                TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);
                if (CheckDateNTime.check(datePicker, timePicker, RideLaterFragment.this.context) == 1) {
                    try {
                        datePicker.setMinDate(System.currentTimeMillis());
                        return;
                    } catch (Exception e) {
                        Toast.makeText(RideLaterFragment.this.context, R.string.time_already_passed, 0).show();
                        return;
                    }
                }
                CheckDateNTime.make_time(timePicker, datePicker);
                RideLaterFragment.this.date.setText(LoginDetails.UserDate);
                RideLaterFragment.this.time.setText(LoginDetails.UserTime);
                Log.e("Correct", "TIME SELECTED");
                alertDialog.dismiss();
            }
        });
    }

    private void setData() {
        try {
            this.currentloc.setText(this.userData.getAddress());
            if (this.userData.getCab_type().equalsIgnoreCase("1")) {
                this.cab_type.setText(R.string.cab1);
            } else if (this.userData.getCab_type().equalsIgnoreCase("2")) {
                this.cab_type.setText(R.string.cab2);
            } else {
                this.cab_type.setText(R.string.cab3);
            }
            this.destinationloc.setText(this.userData.getDesti_address());
            this.distance.setText(this.userData.getDistance());
            this.driver_name.setText(DriverDetails.getDriverName());
            this.driver_number.setText(DriverDetails.getDriverNumber());
            this.cab_number.setText(DriverDetails.getCabNumber());
            this.fare.setText(getResources().getString(R.string.currency_sign) + " " + DriverDetails.getFarePerUnit());
            this.fare_approx.setText(getResources().getString(R.string.currency_sign) + " " + DriverDetails.getFare());
            this.fareRideLaterLayout.setVisibility(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void backPressed() {
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pickup_time_RL:
                selectDateAndTime();
                return;
            case R.id.pick_up_date_RL:
                selectDateAndTime();
                return;
            case R.id.driver_contact_num_RideLater:
                makeCall();
                return;
            case R.id.confirm_img_user_rl:
                if (this.checkConnectivity.isNetworkAvailable()) {
                    try {
                        new ConfirmRejectCabService(this.context, 1, this.context).execute(new String[]{IWebConstant.CONFIRM});
                        this.confirmnow_button.setVisibility(4);
                        this.reject_button.setVisibility(4);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                }
                return;
            case R.id.reject_img_user_rl:
                Toast.makeText(this.context, R.string.rejecting_msg, 1).show();
                new ConfirmRejectCabService(this.context, 2, this.context).execute(new String[]{IWebConstant.REJECT});
                this.confirmnow_button.setVisibility(4);
                this.reject_button.setVisibility(4);
                return;
            case R.id.booknowbutton_RL:
                if (this.checkConnectivity.isNetworkAvailable()) {
                    try {
                        new RideLaterService(this.context).execute(new String[0]);
                        Builder builder = new Builder(this.context);
                        builder.setTitle(getResources().getString(R.string.messagedialogtitle)).setMessage(getResources().getString(R.string.messagedialogmessage)).setCancelable(false).setNegativeButton((int) R.string.ok, new C03752());
                        builder.create().show();
                        return;
                    } catch (NotFoundException e2) {
                        e2.printStackTrace();
                        return;
                    }
                }
                return;
            default:
                return;
        }
    }

    private void makeCall() {
        try {
            this.phoneNumber = "tel:" + DriverDetails.getDriverNumber();
            this.context.startActivity(new Intent("android.intent.action.CALL", Uri.parse(this.phoneNumber)));
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }
}
