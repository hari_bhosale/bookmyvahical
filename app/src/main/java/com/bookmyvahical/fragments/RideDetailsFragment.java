package com.bookmyvahical.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.Toast;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.helper.MyPayPalService;
import com.bookmyvahical.helper.MyWebViewClient;
import com.bookmyvahical.services.UpdatePaymentDetails;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import com.paypal.android.sdk.payments.PayPalService;
import org.json.JSONObject;

public class RideDetailsFragment extends BaseFragment {
    public static String table_id = "";
    private Context context;
    Editor editor;
    JSONObject js;
    private Handler mHandler = new Handler();
    ImageButton paypalbutton;
    SharedPreferences preferences;
    WebView rides_webview;

    final class DemoJavaScriptInterface {
        DemoJavaScriptInterface() {
        }

        @JavascriptInterface
        public void clickOnMakePayment(final String amount, final String payment_id, String currency) {
            RideDetailsFragment.this.mHandler.post(new Runnable() {
                public void run() {
                    try {
                        if (Float.parseFloat(amount) > 0.0f) {
                            RideDetailsFragment.this.makePaymentDialog(payment_id);
                        } else {
                            Toast.makeText(RideDetailsFragment.this.context, "Amount Should be Greater than 0", 0).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @JavascriptInterface
        public void trackDriver(final String driver_Id) {
            RideDetailsFragment.this.mHandler.post(new Runnable() {
                public void run() {
                    try {
                        TrackDriverFragment trackDriverFragment = new TrackDriverFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("driver_id", driver_Id);
                        trackDriverFragment.setArguments(bundle);
                        ((MainActivity) RideDetailsFragment.this.context).getSupportFragmentManager().beginTransaction().replace(R.id.containerView, trackDriverFragment).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    final class MyWebChromeClient extends WebChromeClient {
        MyWebChromeClient() {
        }

        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            result.confirm();
            return true;
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_ridedetails, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.item_two));
        this.context = getActivity();
        try {
            MyPayPalService.startPayPalConfigurationsService(this.context);
            this.rides_webview = (WebView) root.findViewById(R.id.rides_webview);
            MyWebViewClient.enableWebViewSettings(this.rides_webview);
            this.rides_webview.setWebChromeClient(new MyWebChromeClient());
            this.rides_webview.addJavascriptInterface(new DemoJavaScriptInterface(), SystemMediaRouteProvider.PACKAGE_NAME);
            this.rides_webview.setWebViewClient(new MyWebViewClient(this.context));
            if (SharedPreferencesUtility.loadUserType(this.context).equalsIgnoreCase("driver")) {
                this.rides_webview.loadUrl(ProjectURls.RIDES_Driver_URL + SharedPreferencesUtility.loadUsername(this.context));
                Log.d("Ride Url", ProjectURls.RIDES_Driver_URL + SharedPreferencesUtility.loadUsername(this.context));
            } else {
                this.rides_webview.loadUrl(ProjectURls.RIDES_Passenger_URL + SharedPreferencesUtility.loadUsername(this.context));
                Log.d("Ride Url", "" + ProjectURls.RIDES_Passenger_URL + SharedPreferencesUtility.loadUsername(this.context));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    private void makePaymentDialog(final String payment_id) {
        Builder builder = new Builder(this.context);
        builder.setMessage((int) R.string.payment_successful).setTitle((CharSequence) "Thank You").setCancelable(false).setPositiveButton((CharSequence) "OK", new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                new UpdatePaymentDetails(RideDetailsFragment.this.context, RideDetailsFragment.this.rides_webview, payment_id).execute(new String[]{""});
            }
        });
        builder.create().show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.js = MyPayPalService.processPayPalResponse(this.context, requestCode, resultCode, data, this.rides_webview);
        if (this.js == null) {
            this.rides_webview.loadUrl(ProjectURls.PAYMENT_FAIL_URL);
        }
    }

    public void onDestroy() {
        this.context.stopService(new Intent(this.context, PayPalService.class));
        super.onDestroy();
    }

    protected void backPressed() {
        try {
            this.context.startActivity(new Intent(this.context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
