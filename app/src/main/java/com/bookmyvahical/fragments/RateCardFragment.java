package com.bookmyvahical.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.constants.ProjectURls;

public class RateCardFragment extends BaseFragment {
    private Context context;
    ProgressDialog dialog;
    WebView webview;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.web_view_layout, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.item_four));
        this.webview = (WebView) root.findViewById(R.id.web_view);
        this.context = getActivity();
        try {
            setWebViewClient(this.context, this.webview);
            this.webview.loadUrl(ProjectURls.RATE_CARD_PROJECT_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    protected void backPressed() {
        try {
            this.context.startActivity(new Intent(this.context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
