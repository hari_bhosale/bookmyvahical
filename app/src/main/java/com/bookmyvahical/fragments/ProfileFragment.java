package com.bookmyvahical.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.ProgressBar;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.helper.MyWebViewClient;
import com.bookmyvahical.login.LoginDetails;
import com.bookmyvahical.utils.SharedPreferencesUtility;

public class ProfileFragment extends BaseFragment {
    Context context;
    private Handler mHandler = new Handler();
    ProgressBar profileProgressupdateProfile;
    WebView profile_web_view;

    final class DemoJavaScriptInterface {

        /* renamed from: com.bookmyvahical.fragments.ProfileFragment$DemoJavaScriptInterface$1 */
        class C03681 implements Runnable {
            C03681() {
            }

            public void run() {
                ProfileFragment.this.replaceFragment(ProfileFragment.this.context, new UpdateProfilePictureFragment());
            }
        }

        /* renamed from: com.bookmyvahical.fragments.ProfileFragment$DemoJavaScriptInterface$2 */
        class C03692 implements Runnable {
            C03692() {
            }

            public void run() {
            }
        }

        DemoJavaScriptInterface(Context c) {
        }

        @JavascriptInterface
        public void clickOnUploadPic() {
            ProfileFragment.this.mHandler.post(new C03681());
        }

        @JavascriptInterface
        public void clickOnDriver(String fullname, String contact, String cab_type, String cab_number) {
            LoginDetails.CabType = Integer.parseInt(cab_number);
            SharedPreferencesUtility.saveCabType(ProfileFragment.this.context, LoginDetails.CabType);
            ProfileFragment.this.mHandler.post(new C03692());
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.item_one));
        this.context = getActivity();
        try {
            this.profileProgressupdateProfile = (ProgressBar) root.findViewById(R.id.progressBarupdateprofile);
            this.profile_web_view = (WebView) root.findViewById(R.id.profile_webview);
            MyWebViewClient.enableWebViewSettings(this.profile_web_view);
            this.profile_web_view.addJavascriptInterface(new DemoJavaScriptInterface(this.context), "uploadpic");
            this.profileProgressupdateProfile.setVisibility(8);
            this.profile_web_view.setVisibility(0);
            this.profile_web_view.setWebViewClient(new MyWebViewClient(this.context));
            this.profile_web_view.loadUrl(ProjectURls.getProfileUrl(SharedPreferencesUtility.loadUsername(this.context), SharedPreferencesUtility.loadUserType(this.context)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    protected void backPressed() {
        try {
            this.context.startActivity(new Intent(this.context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
