package com.bookmyvahical.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.helper.MyWebViewClient;
import com.bookmyvahical.utils.GPSTracker;

public class HelpFragment extends BaseFragment {
    private Context context;
    GPSTracker gpsTracker;
    private WebView help_webview;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.web_view_layout, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.item_five));
        this.context = getActivity();
        try {
            this.help_webview = (WebView) root.findViewById(R.id.web_view);
            MyWebViewClient.enableWebViewSettings(this.help_webview);
            this.help_webview.setWebViewClient(new MyWebViewClient(this.context));
            this.help_webview.loadUrl(ProjectURls.HELP_PROJECT_URL);
            this.gpsTracker = new GPSTracker(this.context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    protected void backPressed() {
        try {
            this.context.startActivity(new Intent(this.context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
