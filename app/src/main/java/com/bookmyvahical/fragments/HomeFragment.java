package com.bookmyvahical.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;
import com.bookmyvahical.Fcm.CommonUtilities;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.helper.CheckConnectivity;
import com.bookmyvahical.model.DriverDetails;
import com.bookmyvahical.model.UserData;
import com.bookmyvahical.services.NearestCabAsyntask;
import com.bookmyvahical.utils.GPSTracker;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class HomeFragment extends BaseFragment implements OnClickListener, LocationListener, OnCameraChangeListener, OnMapClickListener, OnMapLoadedCallback {
    private static boolean isGPSEnabled;
    private static boolean isNetworkEnabled;
    private static LocationManager locationManager;
    final int BACK_DIALOG = 1;
    private final int MY_PERMISSIONS_REQUEST_LOCATION = 5;
    private final int MY_PERMISSIONS_REQUEST_STOP = 6;
    private String address;
    private LatLng center = null;
    private CheckConnectivity checkConnectivity;
    private Context context;
    ProgressDialog dialog;
    boolean doubleBackToExitPressedOnce = false;
    private GoogleMap googleMap;
    boolean gooleMapFlag = true;
    private GPSTracker gpsTracker;
    private LatLng latLng;
    private ImageButton limozine;
    private final BroadcastReceiver mHandleMessageReceiver = new C03641();
    private ImageButton mapRefreshButton;
    private ImageButton personalCab;
    private ImageButton selectDestinationButton;
    UserData userdata;
    private ImageButton yellowCab;

    /* renamed from: com.bookmyvahical.fragments.HomeFragment$1 */
    class C03641 extends BroadcastReceiver {
        C03641() {
        }

        public void onReceive(Context context, Intent intent) {
            String flag = "" + intent.getExtras().getString("message").charAt(0);
            String newMessage = intent.getExtras().getString("message").substring(1);
        }
    }

    /* renamed from: com.bookmyvahical.fragments.HomeFragment$2 */
    class C03652 implements OnMapLoadedCallback {
        C03652() {
        }

        public void onMapLoaded() {
            if (HomeFragment.this.dialog != null && HomeFragment.this.dialog.isShowing()) {
                HomeFragment.this.dialog.dismiss();
            }
            HomeFragment.this.selectDestinationButton.setVisibility(0);
        }
    }

    /* renamed from: com.bookmyvahical.fragments.HomeFragment$3 */
    class C03663 implements OnCameraChangeListener {
        C03663() {
        }

        public void onCameraChange(CameraPosition cameraPosition) {
            HomeFragment.this.googleMap.clear();
            HomeFragment.this.center = new LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude);
            HomeFragment.this.placeMarker(HomeFragment.this.center);
        }
    }

    /* renamed from: com.bookmyvahical.fragments.HomeFragment$4 */
    class C03674 implements OnMapClickListener {
        C03674() {
        }

        public void onMapClick(LatLng latLng) {
            HomeFragment.this.googleMap.clear();
            HomeFragment.this.center = new LatLng(latLng.latitude, latLng.longitude);
            HomeFragment.this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latLng.latitude, latLng.longitude), 12.0f));
            HomeFragment.this.placeMarker(HomeFragment.this.center);
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        this.context = getActivity();
        try {
            this.userdata = UserData.getinstance(this.context);
            this.checkConnectivity = new CheckConnectivity(this.context);
            this.selectDestinationButton = (ImageButton) root.findViewById(R.id.select_destination_button);
            this.selectDestinationButton.setVisibility(8);
            this.mapRefreshButton = (ImageButton) root.findViewById(R.id.refresh_map);
            this.personalCab = (ImageButton) root.findViewById(R.id.personalcabmapbutton);
            this.yellowCab = (ImageButton) root.findViewById(R.id.yellowcabmapbutton);
            this.limozine = (ImageButton) root.findViewById(R.id.limosinemapbutton);
            this.gpsTracker = new GPSTracker(this.context);
            this.mapRefreshButton.setOnClickListener(this);
            this.selectDestinationButton.setOnClickListener(this);
            this.yellowCab.setOnClickListener(this);
            this.limozine.setOnClickListener(this);
            this.personalCab.setOnClickListener(this);
            Context context = this.context;
            Context context2 = this.context;
            locationManager = (LocationManager) context.getSystemService(Param.LOCATION);
            isGPSEnabled = locationManager.isProviderEnabled("gps");
            isNetworkEnabled = locationManager.isProviderEnabled("network");
            if (isGPSEnabled) {
                locationManager.requestLocationUpdates("gps", 0, 0.0f, this);
            } else {
                locationManager.requestLocationUpdates("network", 0, 0.0f, this);
            }
            if (VERSION.SDK_INT < 23 || ContextCompat.checkSelfPermission(this.context, "android.permission.ACCESS_FINE_LOCATION") == 0 || ContextCompat.checkSelfPermission(this.context, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                initialiseMap();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION"}, 5);
            }
            this.context.registerReceiver(this.mHandleMessageReceiver, new IntentFilter(CommonUtilities.DISPLAY_MESSAGE_ACTION));
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        return root;
    }

    public String getAddress(String lat, String lon) {
        String ret = "";
        try {
            List<Address> addresses = new Geocoder(this.context, Locale.getDefault()).getFromLocation(Double.parseDouble(lat), Double.parseDouble(lon), 1);
            if (addresses == null || addresses.size() <= 0) {
                return "No Address returned!";
            }
            Address returnedAddress = (Address) addresses.get(0);
            StringBuilder strReturnedAddress = new StringBuilder("");
            for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(" ");
            }
            return strReturnedAddress.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "Can't get Address!";
        }
    }

    private void initialiseMap() {
        if (this.checkConnectivity.isNetworkAvailable()) {
            if (this.dialog == null) {
                this.dialog = ProgressDialog.show(this.context, "", getString(R.string.please_wait), true, true);
            }
            try {
                if (this.googleMap == null) {
                    this.googleMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.home_map_fragment)).getMap();
                }
                if (this.googleMap != null) {
                    Location location = this.gpsTracker.getLocation();
                    this.googleMap.setMapType(1);
                    this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12.0f));
                    this.center = new LatLng(location.getLatitude(), location.getLongitude());
                    placeMarker(this.center);
                    changeCamera();
                    return;
                }
                return;
            } catch (Exception w) {
                w.printStackTrace();
                return;
            }
        }
        Toast.makeText(this.context, R.string.internetdisabledmessage, 1).show();
    }

    public void placeMarker(LatLng latLng) {
        if (latLng != null) {
            try {
                String address = getAddress("" + latLng.latitude, "" + latLng.longitude);
                this.latLng = latLng;
                this.googleMap.setMyLocationEnabled(false);
                this.googleMap.addMarker(new MarkerOptions().position(new LatLng(latLng.latitude, latLng.longitude)).title(address)).showInfoWindow();
                this.userdata.setAddress(address);
                this.userdata.setSourc_lat(latLng.latitude);
                this.userdata.setSourc_longt(latLng.longitude);
                new NearestCabAsyntask(this.context, this.googleMap).execute(new String[0]);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }

    private void mapLoaded() {
        if (this.googleMap != null) {
            this.googleMap.setOnMapLoadedCallback(new C03652());
        }
    }

    public void changeCamera() {
        if (this.googleMap != null) {
            this.googleMap.setOnCameraChangeListener(new C03663());
        }
        this.googleMap.setOnMapClickListener(new C03674());
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 5:
                if (grantResults.length > 0 && grantResults[0] == 0) {
                    initialiseMap();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onLocationChanged(Location location) {
    }

    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    public void onProviderEnabled(String s) {
    }

    public void onProviderDisabled(String s) {
    }

    public void onCameraChange(CameraPosition cameraPosition) {
    }

    protected void backPressed() {
        try {
            if (this.doubleBackToExitPressedOnce) {
                try {
                    Intent intent = new Intent("android.intent.action.MAIN");
                    intent.addCategory("android.intent.category.HOME");
                    intent.setFlags(DriveFile.MODE_READ_ONLY);
                    startActivity(intent);
                    System.exit(0);
                    ((MainActivity) getActivity()).finish();
                    Process.killProcess(Process.myPid());
                    super.onDestroy();
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this.context, R.string.tap_again, 0).show();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.yellowcabmapbutton:
                this.userdata.setCab_type("1");
                new NearestCabAsyntask(this.context, this.googleMap).execute(new String[0]);
                this.yellowCab.setImageDrawable(getResources().getDrawable(R.drawable.yellow_cab_clicked));
                this.personalCab.setImageDrawable(getResources().getDrawable(R.drawable.personal_cab));
                this.limozine.setImageDrawable(getResources().getDrawable(R.drawable.limousine));
                return;
            case R.id.personalcabmapbutton:
                this.userdata.setCab_type("2");
                new NearestCabAsyntask(this.context, this.googleMap).execute(new String[0]);
                this.yellowCab.setImageDrawable(getResources().getDrawable(R.drawable.yellow_cab));
                this.personalCab.setImageDrawable(getResources().getDrawable(R.drawable.personal_cab_clicked));
                this.limozine.setImageDrawable(getResources().getDrawable(R.drawable.limousine));
                return;
            case R.id.limosinemapbutton:
                this.userdata.setCab_type("3");
                new NearestCabAsyntask(this.context, this.googleMap).execute(new String[0]);
                this.yellowCab.setImageDrawable(getResources().getDrawable(R.drawable.yellow_cab));
                this.personalCab.setImageDrawable(getResources().getDrawable(R.drawable.personal_cab));
                this.limozine.setImageDrawable(getResources().getDrawable(R.drawable.limousine_clicked));
                return;
            case R.id.select_destination_button:
                String sourceAddress = this.userdata.getAddress();
                if (DriverDetails.getNoCabFound().equalsIgnoreCase("NoCabFound")) {
                    Toast.makeText(this.context, "No Cab Available Right Now", 0).show();
                    return;
                } else if (sourceAddress.equalsIgnoreCase("Can't get Address!") || sourceAddress.equalsIgnoreCase(" ")) {
                    Toast.makeText(this.context, "First Select Source Address", 0).show();
                    return;
                } else {
                    replaceFragment(this.context, new SelectDestinationFragment());
                    return;
                }
            case R.id.refresh_map:
                if (this.googleMap != null) {
                    try {
                        Toast.makeText(this.context, R.string.please_wait, 0).show();
                        this.googleMap.clear();
                        initialiseMap();
                        return;
                    } catch (SecurityException e) {
                        e.printStackTrace();
                        return;
                    }
                }
                return;
            default:
                return;
        }
    }

    public void onMapClick(LatLng latLng) {
    }

    public void onMapLoaded() {
    }

    public void onPause() {
        super.onPause();
        try {
            System.gc();
            locationManager.removeUpdates(this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void onStop() {
        super.onStop();
        if (VERSION.SDK_INT < 23 || ContextCompat.checkSelfPermission(this.context, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            locationManager.removeUpdates(this);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 6);
        }
        try {
            this.context.unregisterReceiver(this.mHandleMessageReceiver);
        } catch (Exception e) {
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager fm = getChildFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.home_map_fragment);
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fragment);
        ft.commitAllowingStateLoss();
    }

    public void onResume() {
        super.onResume();
        try {
            mapLoaded();
        } catch (Exception e) {
            Toast.makeText(this.context, R.string.map_expception, 0).show();
            e.printStackTrace();
        }
    }
}
