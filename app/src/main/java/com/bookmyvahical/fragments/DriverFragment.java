package com.bookmyvahical.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.bookmyvahical.Fcm.CommonUtilities;
import com.bookmyvahical.alertspinner.iAm_CustomList;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.constants.AppConstants;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.latlong.LatLongDetails;
import com.bookmyvahical.login.LoginDetails;
import com.bookmyvahical.login.PassengerDetails;
import com.bookmyvahical.model.DriverDetails;
import com.bookmyvahical.services.ConfirmRejectCabService;
import com.bookmyvahical.services.DriverStatusUpdate;
import com.bookmyvahical.services.GetDriverStatus;
import com.bookmyvahical.utils.GPSTracker;
import com.google.android.gms.drive.DriveFile;

public class DriverFragment extends BaseFragment implements OnClickListener {
    private boolean First_time_flage;
    private LinearLayout button_layout;
    ImageButton confirm_driver;
    private Context context;
    private LinearLayout details_layout;
    public Dialog dialog;
    boolean doubleBackToExitPressedOnce = false;
    private ImageView driverStatusimageview;
    private TextView driverStatustextview;
    private LatLongDetails driver_latlongobj = new LatLongDetails();
    private TextView drop_loc;
    private TextView fare_value;
    private String first_time_tag = "first_time_flag";
    private GPSTracker gpsTracker;
    private ImageView imageViewPhone;
    private TextView linear;
    public ListView list;
    private final BroadcastReceiver mHandleMessageReceiver = new C03591();
    private LinearLayout pass_detail_layout;
    private TextView pass_name;
    private TextView pass_num;
    private String phoneNumber;
    private TextView pick_date;
    private TextView pick_date_left;
    private TextView pick_loc;
    private TextView pick_time;
    private TextView pick_time_left;
    ImageButton reject_driver;
    private LinearLayout statuslinearlayout;
    private LinearLayout update_profile;

    /* renamed from: com.bookmyvahical.fragments.DriverFragment$1 */
    class C03591 extends BroadcastReceiver {
        C03591() {
        }

        public void onReceive(Context context, Intent intent) {
            String[] data;
            int i;
            String flag = "" + intent.getExtras().getString("message").charAt(0);
            String newMessage = intent.getExtras().getString("message").substring(1);
            if (flag.contains("2")) {
                try {
                    data = new String[newMessage.split("]").length];
                    data = newMessage.split("]");
                    for (i = 0; i < data.length; i++) {
                        data[i] = data[i].replace("[", "");
                        Log.e("Data" + i, " " + data[i].toString());
                    }
                    PassengerDetails.passengername = data[0];
                    PassengerDetails.passengernumber = data[1];
                    PassengerDetails.passemger_pick_date = data[2];
                    PassengerDetails.passenger_pick_time = data[3];
                    PassengerDetails.passenger_pick_loc = data[4];
                    PassengerDetails.passenger_drop_loc = data[5];
                    LoginDetails.Unique_Table_ID = data[6];
                    PassengerDetails.fare = data[7];
                    DriverFragment.this.details_layout.setVisibility(0);
                    DriverFragment.this.pass_detail_layout.setVisibility(0);
                    DriverFragment.this.button_layout.setVisibility(0);
                    DriverFragment.this.fare_value.setVisibility(0);
                    DriverFragment.this.pick_date_left.setVisibility(0);
                    DriverFragment.this.pick_time_left.setVisibility(0);
                    DriverFragment.this.pick_date.setVisibility(0);
                    DriverFragment.this.pick_time.setVisibility(0);
                    DriverFragment.this.pass_name.setText("" + PassengerDetails.passengername);
                    DriverFragment.this.pass_num.setText("" + PassengerDetails.passengernumber);
                    DriverFragment.this.pick_loc.setText("" + PassengerDetails.passenger_pick_loc);
                    DriverFragment.this.drop_loc.setText("" + PassengerDetails.passenger_drop_loc);
                    DriverFragment.this.pick_date.setText("" + PassengerDetails.passemger_pick_date);
                    DriverFragment.this.pick_time.setText("" + PassengerDetails.passenger_pick_time);
                    DriverFragment.this.fare_value.setText(context.getResources().getString(R.string.currency_sign) + " " + PassengerDetails.fare);
                    DriverFragment.this.driverStatusimageview.setImageResource(R.drawable.pending);
                    DriverFragment.this.driverStatustextview.setText(R.string.pending_status);
                    DriverDetails.setDriverStatus("" + DriverFragment.this.getResources().getString(R.string.pending_status));
                    new DriverStatusUpdate((Activity) context).execute(new String[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (flag.contains("3")) {
                try {
                    Toast.makeText(context, "" + newMessage, 0).show();
                    DriverFragment.this.pass_detail_layout.setVisibility(4);
                    DriverFragment.this.button_layout.setVisibility(4);
                    DriverFragment.this.details_layout.setVisibility(4);
                    PassengerDetails.passengername = "";
                    PassengerDetails.passengernumber = "";
                    PassengerDetails.passemger_pick_date = "";
                    PassengerDetails.passenger_pick_time = "";
                    PassengerDetails.passenger_pick_loc = "";
                    PassengerDetails.passenger_drop_loc = "";
                    DriverFragment.this.driverStatusimageview.setImageResource(R.drawable.available);
                    DriverDetails.setDriverStatus("" + DriverFragment.this.getResources().getString(R.string.available_status));
                    DriverFragment.this.driverStatustextview.setText(DriverDetails.getDriverStatus());
                    new DriverStatusUpdate((Activity) context).execute(new String[0]);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (flag.contains("4")) {
                try {
                    data = new String[newMessage.split("]").length];
                    data = newMessage.split("]");
                    for (i = 0; i < data.length; i++) {
                        data[i] = data[i].replace("[", "");
                        Log.e("Data" + i, " " + data[i].toString());
                    }
                    PassengerDetails.passengername = data[0];
                    PassengerDetails.passengernumber = data[1];
                    PassengerDetails.passemger_pick_date = data[2];
                    PassengerDetails.passenger_pick_time = data[3];
                    PassengerDetails.passenger_pick_loc = data[4];
                    PassengerDetails.passenger_drop_loc = data[5];
                    LoginDetails.Unique_Table_ID = data[6];
                    PassengerDetails.fare = data[7];
                    DriverFragment.this.pass_detail_layout.setVisibility(0);
                    DriverFragment.this.button_layout.setVisibility(0);
                    DriverFragment.this.pass_name.setText("" + PassengerDetails.passengername);
                    DriverFragment.this.pass_num.setText("" + PassengerDetails.passengernumber);
                    DriverFragment.this.pick_loc.setText("" + PassengerDetails.passenger_pick_loc);
                    DriverFragment.this.drop_loc.setText("" + PassengerDetails.passenger_drop_loc);
                    DriverFragment.this.pick_date_left.setVisibility(0);
                    DriverFragment.this.pick_time_left.setVisibility(0);
                    DriverFragment.this.details_layout.setVisibility(0);
                    DriverFragment.this.fare_value.setVisibility(0);
                    DriverFragment.this.pick_date.setVisibility(0);
                    DriverFragment.this.pick_date.setText("" + PassengerDetails.passemger_pick_date);
                    DriverFragment.this.pick_time.setText("" + PassengerDetails.passenger_pick_time);
                    DriverFragment.this.fare_value.setText(context.getResources().getString(R.string.currency_sign) + " " + PassengerDetails.fare);
                    DriverFragment.this.pick_time.setVisibility(0);
                    DriverFragment.this.driverStatusimageview.setImageResource(R.drawable.pending);
                    DriverFragment.this.driverStatustextview.setText(R.string.pending_status);
                    DriverDetails.setDriverStatus("" + DriverFragment.this.getResources().getString(R.string.pending));
                    new DriverStatusUpdate((Activity) context).execute(new String[0]);
                } catch (Exception e22) {
                    e22.printStackTrace();
                }
            }
            if (flag.contains("5")) {
                try {
                    Toast.makeText(context, "" + newMessage, 0).show();
                    DriverFragment.this.pass_detail_layout.setVisibility(4);
                    DriverFragment.this.button_layout.setVisibility(4);
                    DriverFragment.this.details_layout.setVisibility(4);
                    PassengerDetails.passengername = "";
                    PassengerDetails.passengernumber = "";
                    PassengerDetails.passemger_pick_date = "";
                    PassengerDetails.passenger_pick_time = "";
                    PassengerDetails.passenger_pick_loc = "";
                    PassengerDetails.passenger_drop_loc = "";
                    DriverFragment.this.driverStatusimageview.setImageResource(R.drawable.available);
                    DriverDetails.setDriverStatus("" + DriverFragment.this.getResources().getString(R.string.available_status));
                    DriverFragment.this.driverStatustextview.setText(DriverDetails.getDriverStatus());
                    new DriverStatusUpdate((Activity) context).execute(new String[0]);
                } catch (Exception e222) {
                    e222.printStackTrace();
                }
            }
            if (flag.contains("7")) {
                try {
                    Toast.makeText(context, "" + newMessage, 1).show();
                } catch (Exception e2222) {
                    e2222.printStackTrace();
                }
            }
            if (flag.contains("8")) {
                try {
                    DriverFragment.this.driverStatusimageview.setImageResource(R.drawable.booked);
                    DriverDetails.setDriverStatus("2131165269");
                    DriverFragment.this.driverStatustextview.setText(DriverDetails.getDriverStatus());
                    new DriverStatusUpdate((Activity) context).execute(new String[0]);
                    DriverFragment.this.replaceFragment(context, new RideDetailsFragment());
                } catch (Exception e22222) {
                    e22222.printStackTrace();
                }
            }
            if (flag.contains("a")) {
                DriverFragment.this.showDialogBox();
            }
        }
    }

    /* renamed from: com.bookmyvahical.fragments.DriverFragment$2 */
    class C03602 implements DialogInterface.OnClickListener {
        C03602() {
        }

        public void onClick(DialogInterface arg0, int arg1) {
        }
    }

    /* renamed from: com.bookmyvahical.fragments.DriverFragment$3 */
    class C03613 implements DialogInterface.OnClickListener {
        C03613() {
        }

        public void onClick(DialogInterface arg0, int arg1) {
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_driver, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle((int) R.string.cabbooking_driver);
        try {
            this.context = getActivity();
            DriverDetails.setDriverStatus("");
            SharedPreferences myPreferences = this.context.getSharedPreferences("myprefreferences", 0);
            this.statuslinearlayout = (LinearLayout) root.findViewById(R.id.driverstatusllid);
            this.driverStatusimageview = (ImageView) root.findViewById(R.id.driverstatusimgid);
            this.pass_detail_layout = (LinearLayout) root.findViewById(R.id.passenger_layout_id);
            this.button_layout = (LinearLayout) root.findViewById(R.id.driver_buttons_linear_id);
            this.pass_name = (TextView) root.findViewById(R.id.cust_name_dri);
            this.pass_num = (TextView) root.findViewById(R.id.cust_num_dri);
            this.pick_loc = (TextView) root.findViewById(R.id.pick_loc_dri);
            this.drop_loc = (TextView) root.findViewById(R.id.drop_loc_dri);
            this.pick_time = (TextView) root.findViewById(R.id.pick_time_dri);
            this.pick_date = (TextView) root.findViewById(R.id.pick_date_dri);
            this.fare_value = (TextView) root.findViewById(R.id.fare_value);
            this.pick_date_left = (TextView) root.findViewById(R.id.pick_up_date_tex);
            this.pick_time_left = (TextView) root.findViewById(R.id.pick_up_time_tex);
            this.driverStatustextview = (TextView) root.findViewById(R.id.driverstatustextid);
            this.imageViewPhone = (ImageView) root.findViewById(R.id.imageViewPhone);
            this.confirm_driver = (ImageButton) root.findViewById(R.id.confirm_img_dri);
            this.reject_driver = (ImageButton) root.findViewById(R.id.reject_img_dri);
            this.details_layout = (LinearLayout) root.findViewById(R.id.details_layout);
            this.update_profile = (LinearLayout) root.findViewById(R.id.updateprofile);
            this.imageViewPhone.setOnClickListener(this);
            setLinearLayoutOnClickListener(this.context, this.statuslinearlayout, this.driverStatusimageview, this.driverStatustextview);
            this.First_time_flage = myPreferences.getBoolean(this.first_time_tag, true);
            if (this.First_time_flage) {
                this.update_profile.setVisibility(0);
                this.First_time_flage = false;
                Editor editor = myPreferences.edit();
                editor.putBoolean(this.first_time_tag, this.First_time_flage);
                editor.commit();
            }
            new GetDriverStatus(this.context, this.driverStatusimageview, this.driverStatustextview, this.statuslinearlayout).execute(new String[0]);
            this.confirm_driver.setOnClickListener(this);
            this.reject_driver.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        get_driver_location();
        return root;
    }

    private LatLongDetails get_driver_location() {
        this.gpsTracker = new GPSTracker(this.context);
        LatLongDetails temp_latlongobj = new LatLongDetails();
        if (this.gpsTracker.canGetLocation()) {
            try {
                LatLongDetails.driver_latitude = this.gpsTracker.getLocation().getLatitude();
                LatLongDetails.driver_longitude = this.gpsTracker.getLocation().getLongitude();
            } catch (Exception e) {
                Toast.makeText(this.context, R.string.gps_not_enabled, 0).show();
                e.printStackTrace();
            }
        } else {
            Log.d("Gps", "null in driver activity");
        }
        return temp_latlongobj;
    }

    public void makeCall() {
        try {
            this.phoneNumber = "tel:" + PassengerDetails.passengernumber;
            this.context.startActivity(new Intent("android.intent.action.CALL", Uri.parse(this.phoneNumber)));
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void showDialogBox() {
        Builder alertDialogBuilder = new Builder(this.context);
        alertDialogBuilder.setMessage(this.context.getResources().getString(R.string.cash_payment));
        alertDialogBuilder.setPositiveButton((CharSequence) "Ok", new C03602());
        alertDialogBuilder.setNegativeButton((CharSequence) "Cancel", new C03613());
        alertDialogBuilder.create().show();
    }

    protected void backPressed() {
        try {
            if (this.doubleBackToExitPressedOnce) {
                try {
                    Intent intent = new Intent("android.intent.action.MAIN");
                    intent.addCategory("android.intent.category.HOME");
                    intent.setFlags(DriveFile.MODE_READ_ONLY);
                    startActivity(intent);
                    System.exit(0);
                    ((MainActivity) getActivity()).finish();
                    Process.killProcess(Process.myPid());
                    super.onDestroy();
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this.context, R.string.tap_again, 0).show();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void onDestroy() {
        try {
            this.context.unregisterReceiver(this.mHandleMessageReceiver);
        } catch (Exception e) {
            Log.e("UnRegister Receiver Error", "> " + e.getMessage());
        }
        super.onDestroy();
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        try {
            this.context.registerReceiver(this.mHandleMessageReceiver, new IntentFilter(CommonUtilities.DISPLAY_MESSAGE_ACTION));
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewPhone:
                makeCall();
                return;
            case R.id.confirm_img_dri:
                this.driverStatustextview.setText(R.string.booked_status);
                this.driverStatusimageview.setImageResource(R.drawable.booked);
                this.button_layout.setVisibility(4);
                DriverDetails.setDriverStatus("" + getResources().getString(R.string.booked_status));
                new DriverStatusUpdate(this.context).execute(new String[0]);
                new ConfirmRejectCabService(getActivity(), 1, this.context).execute(new String[]{IWebConstant.CONFIRM});
                return;
            case R.id.reject_img_dri:
                Toast.makeText(this.context, R.string.please_wait, 0).show();
                this.details_layout.setVisibility(4);
                this.pass_detail_layout.setVisibility(4);
                this.button_layout.setVisibility(4);
                this.driverStatustextview.setText(R.string.available_status);
                this.driverStatusimageview.setImageResource(R.drawable.available);
                DriverDetails.setDriverStatus("" + getResources().getString(R.string.available_status));
                new DriverStatusUpdate(this.context).execute(new String[0]);
                new ConfirmRejectCabService(getActivity(), 1, this.context).execute(new String[]{IWebConstant.REJECT});
                return;
            default:
                return;
        }
    }

    private void setLinearLayoutOnClickListener(final Context context, LinearLayout statuslinearlayout, final ImageView driverStatusimageview, final TextView driverStatustextview) {
        statuslinearlayout.setOnClickListener(new OnClickListener() {

            /* renamed from: com.bookmyvahical.fragments.DriverFragment$4$1 */
            class C03621 implements OnItemClickListener {
                C03621() {
                }

                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    switch (position) {
                        case 0:
                            driverStatustextview.setText(R.string.available_status);
                            driverStatusimageview.setImageResource(R.drawable.available);
                            DriverDetails.setDriverStatus("" + DriverFragment.this.getResources().getString(R.string.available_status));
                            break;
                        case 1:
                            driverStatustextview.setText(R.string.booked_status);
                            driverStatusimageview.setImageResource(R.drawable.booked);
                            DriverDetails.setDriverStatus("" + DriverFragment.this.getResources().getString(R.string.booked_status));
                            break;
                        case 2:
                            driverStatustextview.setText(R.string.dnd_status);
                            driverStatusimageview.setImageResource(R.drawable.dnd);
                            DriverDetails.setDriverStatus("" + DriverFragment.this.getResources().getString(R.string.dnd_status));
                            break;
                    }
                    new DriverStatusUpdate((Activity) context).execute(new String[]{""});
                    Log.e("Item clicked at ", "" + position);
                    DriverFragment.this.dialog.dismiss();
                }
            }

            public void onClick(View v) {
                DriverFragment.this.dialog = new Dialog(context);
                DriverFragment.this.dialog.requestWindowFeature(1);
                DriverFragment.this.dialog.setContentView(R.layout.iamlistspinner);
                DriverFragment.this.dialog.setTitle("I am .....");
                iAm_CustomList listAdapter = new iAm_CustomList((Activity) context, AppConstants.catgories, AppConstants.imageids);
                DriverFragment.this.list = (ListView) DriverFragment.this.dialog.findViewById(R.id.iam_listview);
                DriverFragment.this.list.setAdapter(listAdapter);
                DriverFragment.this.list.setOnItemClickListener(new C03621());
                DriverFragment.this.dialog.show();
            }
        });
    }
}
