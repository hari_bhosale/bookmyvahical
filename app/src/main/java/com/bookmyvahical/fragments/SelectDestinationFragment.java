package com.bookmyvahical.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.bookmyvahical.R;
import com.bookmyvahical.model.UserData;
import com.bookmyvahical.services.DrawrootTask;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocomplete.IntentBuilder;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class SelectDestinationFragment extends BaseFragment implements OnClickListener {
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    public static final int RESULT_CANCELED = 0;
    public static final int RESULT_OK = -1;
    private ImageButton bookNowButton;
    private Context context;
    private TextView destinationText_SD;
    private GoogleMap googleMap;
    private LatLng location;
    private LatLng mapCenter;
    private TextView set_source_SD;
    private Double sourceLat;
    private Double sourceLong;
    private UserData userData;

    /* renamed from: com.bookmyvahical.fragments.SelectDestinationFragment$1 */
    class C03761 implements OnMapClickListener {
        C03761() {
        }

        public void onMapClick(LatLng latLng) {
            try {
                SelectDestinationFragment.this.googleMap.clear();
                SelectDestinationFragment.this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latLng.latitude, latLng.longitude), 12.0f));
                String address = SelectDestinationFragment.this.getAddress("" + latLng.latitude, "" + latLng.longitude);
                SelectDestinationFragment.this.destinationText_SD.setText(address);
                SelectDestinationFragment.this.userData.setDesti_address(address);
                SelectDestinationFragment.this.userData.setDest_lat(latLng.latitude);
                SelectDestinationFragment.this.userData.setDest_longt(latLng.longitude);
                SelectDestinationFragment.this.drawRootFromSourcToDestination(new LatLng(SelectDestinationFragment.this.sourceLat.doubleValue(), SelectDestinationFragment.this.sourceLong.doubleValue()), latLng);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_select_destination, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle((int) R.string.item_select_destination);
        try {
            this.context = getActivity();
            this.userData = UserData.getinstance(this.context);
            this.destinationText_SD = (TextView) root.findViewById(R.id.destination_SD);
            this.bookNowButton = (ImageButton) root.findViewById(R.id.book_now_Imagebutton_SD);
            this.set_source_SD = (TextView) root.findViewById(R.id.source_textview_SD);
            this.set_source_SD.setText(this.userData.getAddress());
            this.sourceLat = Double.valueOf(this.userData.getSourc_lat());
            this.sourceLong = Double.valueOf(this.userData.getSourc_longt());
            this.userData.setDesti_address("");
            setClickListner();
            initialisemap();
            onMapClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!(VERSION.SDK_INT < 23 || ContextCompat.checkSelfPermission(this.context, "android.permission.ACCESS_FINE_LOCATION") == 0 || ContextCompat.checkSelfPermission(this.context, "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION"}, 1);
        }
        return root;
    }

    private void onMapClick() {
        this.googleMap.setOnMapClickListener(new C03761());
    }

    private void setClickListner() {
        try {
            this.bookNowButton.setOnClickListener(this);
            this.destinationText_SD.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initialisemap() {
        try {
            this.googleMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapfragment_SD)).getMap();
            if (this.googleMap != null) {
                this.location = new LatLng(this.sourceLat.doubleValue(), this.sourceLong.doubleValue());
                this.mapCenter = this.location;
                if (this.location != null) {
                    this.googleMap.setMapType(1);
                    this.googleMap.addMarker(new MarkerOptions().position(this.location).title(this.userData.getAddress()).icon(BitmapDescriptorFactory.fromResource(R.drawable.source)));
                    this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(this.mapCenter, 12.0f));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void drawRootFromSourcToDestination(LatLng source, LatLng destination) {
        try {
            this.googleMap.clear();
            placeMarker(source, "1");
            placeMarker(destination, "2");
            new DrawrootTask(this.context, source, destination, this.googleMap, this.bookNowButton).execute(new String[0]);
            String address = "";
            String lat = "" + destination.latitude;
            String longt = "" + destination.longitude;
            this.userData.setDest_lat(destination.latitude);
            this.userData.setDest_longt(destination.longitude);
            this.userData.setDesti_address(getAddress(lat, longt));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getAddress(String lat, String lon) {
        String ret = "";
        try {
            List<Address> addresses = new Geocoder(this.context, Locale.getDefault()).getFromLocation(Double.parseDouble(lat), Double.parseDouble(lon), 1);
            if (addresses == null || addresses.size() <= 0) {
                return "No Address returned!";
            }
            Address returnedAddress = (Address) addresses.get(0);
            StringBuilder strReturnedAddress = new StringBuilder("");
            for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(" ");
            }
            return strReturnedAddress.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "Can't get Address!";
        }
    }

    public void placeMarker(LatLng latlong, String str) {
        try {
            if (str.equalsIgnoreCase("1")) {
                this.googleMap.addMarker(new MarkerOptions().position(new LatLng(latlong.latitude, latlong.longitude)).snippet(String.valueOf("")).draggable(true).title(this.userData.getAddress()).icon(BitmapDescriptorFactory.fromResource(R.drawable.source)));
            } else {
                this.googleMap.addMarker(new MarkerOptions().position(new LatLng(latlong.latitude, latlong.longitude)).snippet(String.valueOf("")).draggable(true).title(this.userData.getDesti_address()).icon(BitmapDescriptorFactory.fromResource(R.drawable.destination)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void backPressed() {
        replaceFragment(this.context, new HomeFragment());
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.destination_SD:
                callPlaceAutocompleteActivityIntent();
                return;
            case R.id.book_now_Imagebutton_SD:
                bookNow();
                return;
            default:
                return;
        }
    }

    private void bookNow() {
        RuntimeException e;
        try {
            if (this.userData.getDesti_address().equalsIgnoreCase("")) {
                Toast.makeText(this.context, R.string.select_destination_error, 0).show();
            } else if (Double.parseDouble(this.userData.getDistance().replaceAll("[km m]", "").replaceAll("m", "")) < 2.0d) {
                Toast.makeText(this.context, R.string.short_distance, 0).show();
            } else {
                replaceFragment(this.context, new RideFragment());
            }
        } catch (NumberFormatException e2) {
            e = e2;
            e.printStackTrace();
        } catch (NotFoundException e3) {
            e = e3;
            e.printStackTrace();
        }
    }

    private void callPlaceAutocompleteActivityIntent() {
        try {
            startActivityForResult(new IntentBuilder(1).build((Activity) this.context), 1);
        } catch (GooglePlayServicesRepairableException e) {
        } catch (GooglePlayServicesNotAvailableException e2) {
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager fm = getChildFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.mapfragment_SD);
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fragment);
        ft.commitAllowingStateLoss();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != 1) {
            return;
        }
        if (resultCode == -1) {
            try {
                Place place = PlaceAutocomplete.getPlace(this.context, data);
                this.destinationText_SD.setText(place.getAddress());
                LatLng latLng = place.getLatLng();
                this.userData.setDesti_address("" + place.getAddress());
                this.userData.setDest_lat(latLng.latitude);
                this.userData.setDest_longt(latLng.longitude);
                drawRootFromSourcToDestination(new LatLng(this.sourceLat.doubleValue(), this.sourceLong.doubleValue()), latLng);
                Log.i("tag", "Place:" + place.toString());
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("exception", "" + e);
            }
        } else if (resultCode == 2) {
            Log.i("tag", PlaceAutocomplete.getStatus(this.context, data).getStatusMessage());
        } else if (requestCode != 0) {
        }
    }
}
