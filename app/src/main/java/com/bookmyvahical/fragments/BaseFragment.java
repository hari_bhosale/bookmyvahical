package com.bookmyvahical.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.webkit.WebView;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.helper.MyWebViewClient;

public abstract class BaseFragment extends Fragment {

    /* renamed from: com.bookmyvahical.fragments.BaseFragment$1 */
    class MyClickListener implements OnKeyListener {
        MyClickListener() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != 1 || keyCode != 4) {
                return false;
            }
            BaseFragment.this.backPressed();
            return true;
        }
    }

    protected abstract void backPressed();

    public void onStop() {
        System.gc();
        super.onStop();
    }

    public void replaceFragment(Context context, Fragment fragment) {
        ((MainActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.containerView, fragment).commit();
    }

    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new MyClickListener());
    }

    public void setWebViewClient(Context context, WebView web) {
        web.setWebViewClient(new MyWebViewClient(context));
        MyWebViewClient.enableWebViewSettings(web);
    }
}
