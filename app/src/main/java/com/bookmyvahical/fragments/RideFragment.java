package com.bookmyvahical.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.bookmyvahical.R;
import com.bookmyvahical.constants.IWebConstant;
import com.bookmyvahical.helper.CheckConnectivity;
import com.bookmyvahical.model.DriverDetails;
import com.bookmyvahical.model.UserData;
import com.bookmyvahical.services.ConfirmRejectCabService;
import com.bookmyvahical.services.RideNowConfirmService;
import com.bookmyvahical.services.Update_user_loc_task;

public class RideFragment extends BaseFragment implements OnClickListener {
    public static boolean flag = false;
    private TextView cab_num;
    private TextView cab_time;
    private TextView cab_type;
    private CheckConnectivity checkConnectivity;
    private ImageButton confirm;
    private TextView contact;
    private Context context;
    private TextView destination;
    private TextView dis_time;
    private TextView distance;
    private TextView driver_name;
    private TextView fare;
    private TextView fareApprox;
    private LinearLayout fareDistanceRideLayout;
    private LinearLayout getting_cabdetailsLL;
    private LinearLayout got_cabDetailsLL;
    private String phoneNumber;
    private ProgressBar progress;
    private TextView progress_text_view;
    private ImageButton reject;
    private ImageButton ride_later;
    private ImageButton ride_now;
    private LinearLayout ridenowlaterlayout;
    private TextView source;
    private TextView travelTime;
    private UserData userData;

    /* renamed from: com.bookmyvahical.fragments.RideFragment$1 */
    class C03731 implements DialogInterface.OnClickListener {
        C03731() {
        }

        public void onClick(DialogInterface dialog, int id) {
            new RideNowConfirmService(RideFragment.this.context).execute(new String[0]);
            RideFragment.this.ride_now.setVisibility(8);
            RideFragment.this.ride_later.setVisibility(8);
            RideFragment.this.confirm.setVisibility(0);
            RideFragment.this.reject.setVisibility(0);
            dialog.cancel();
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_ride, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle((CharSequence) "Ride");
        try {
            this.context = getActivity();
            this.userData = UserData.getinstance(this.context);
            this.getting_cabdetailsLL = (LinearLayout) root.findViewById(R.id.gettingdetailsLL);
            this.got_cabDetailsLL = (LinearLayout) root.findViewById(R.id.cab_details_layout);
            this.fareDistanceRideLayout = (LinearLayout) root.findViewById(R.id.distance_fare_layout_Ride);
            this.cab_time = (TextView) root.findViewById(R.id.cab_reach_time_Ride);
            this.driver_name = (TextView) root.findViewById(R.id.driver_name_Ride);
            this.contact = (TextView) root.findViewById(R.id.driver_contact_num_Ride);
            this.cab_num = (TextView) root.findViewById(R.id.Cab_num_Ride);
            this.progress_text_view = (TextView) root.findViewById(R.id.cab_details_progress_textview);
            this.progress = (ProgressBar) root.findViewById(R.id.cab_details_progress);
            this.ride_now = (ImageButton) root.findViewById(R.id.ride_now_IMGbutton);
            this.confirm = (ImageButton) root.findViewById(R.id.confirm_IMGbutton);
            this.reject = (ImageButton) root.findViewById(R.id.reject_IB_Ride);
            this.ridenowlaterlayout = (LinearLayout) root.findViewById(R.id.ridenowlaterbuttonlayout);
            this.ride_later = (ImageButton) root.findViewById(R.id.ride_later_IB_Ride);
            this.source = (TextView) root.findViewById(R.id.current_loc_Ride);
            this.cab_type = (TextView) root.findViewById(R.id.cab_type_Ride);
            this.destination = (TextView) root.findViewById(R.id.destination_Ride);
            this.distance = (TextView) root.findViewById(R.id.distance_Ride);
            this.fare = (TextView) root.findViewById(R.id.fare_Ride);
            this.fareApprox = (TextView) root.findViewById(R.id.fare_approx);
            this.travelTime = (TextView) root.findViewById(R.id.travelTime);
            this.checkConnectivity = new CheckConnectivity(this.context);
            this.ride_later.setOnClickListener(this);
            this.ride_now.setOnClickListener(this);
            this.confirm.setOnClickListener(this);
            this.reject.setOnClickListener(this);
            this.contact.setOnClickListener(this);
            new Update_user_loc_task(this.context, this.ridenowlaterlayout, this).execute(new String[0]);
            setData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    public void setData() {
        try {
            this.source.setText(this.userData.getAddress());
            if (this.userData.getCab_type().equalsIgnoreCase("1")) {
                this.cab_type.setText(R.string.cab1);
            } else if (this.userData.getCab_type().equalsIgnoreCase("2")) {
                this.cab_type.setText(R.string.cab2);
            } else {
                this.cab_type.setText(R.string.cab3);
            }
            this.destination.setText(this.userData.getDesti_address());
            this.distance.setText(this.userData.getDistance());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void backPressed() {
        replaceFragment(this.context, new SelectDestinationFragment());
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ride_now_IMGbutton:
                if (this.checkConnectivity.isNetworkAvailable()) {
                    Builder builder = new Builder(this.context);
                    builder.setTitle(getResources().getString(R.string.messagedialogtitle)).setMessage(getResources().getString(R.string.messagedialogmessage)).setCancelable(false).setNegativeButton((CharSequence) "Ok", new C03731());
                    builder.create().show();
                    return;
                }
                return;
            case R.id.ride_later_IB_Ride:
                replaceFragment(this.context, new RideLaterFragment());
                return;
            case R.id.confirm_IMGbutton:
                try {
                    if (this.checkConnectivity.isNetworkAvailable()) {
                        Toast.makeText(this.context, R.string.confirming_msg, 1).show();
                        new ConfirmRejectCabService(getActivity(), 1, this.context).execute(new String[]{IWebConstant.CONFIRM});
                        this.confirm.setVisibility(4);
                        this.reject.setVisibility(4);
                        return;
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            case R.id.reject_IB_Ride:
                try {
                    if (this.checkConnectivity.isNetworkAvailable()) {
                        Toast.makeText(this.context, R.string.rejecting_msg, 1).show();
                        new ConfirmRejectCabService(getActivity(), 2, this.context).execute(new String[]{IWebConstant.REJECT});
                        this.confirm.setVisibility(4);
                        this.reject.setVisibility(4);
                        return;
                    }
                    return;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return;
                }
            case R.id.driver_contact_num_Ride:
                makeCall();
                return;
            default:
                return;
        }
    }

    private void makeCall() {
        try {
            this.phoneNumber = "tel:" + DriverDetails.getDriverNumber();
            this.context.startActivity(new Intent("android.intent.action.CALL", Uri.parse(this.phoneNumber)));
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void responseFirst() {
        try {
            this.progress_text_view.setText(R.string.no_cabs);
            this.progress.setVisibility(4);
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void responseSecond() {
        try {
            this.getting_cabdetailsLL.setVisibility(8);
            this.got_cabDetailsLL.setVisibility(0);
            this.ridenowlaterlayout.setVisibility(0);
            this.fareDistanceRideLayout.setVisibility(0);
            this.cab_time.setText("" + DriverDetails.getNearesCabReachingTime());
            this.driver_name.setText("" + DriverDetails.getDriverName());
            this.contact.setText("" + DriverDetails.getDriverNumber());
            this.cab_num.setText("" + DriverDetails.getCabNumber());
            this.fare.setText(getResources().getString(R.string.currency_sign) + " " + DriverDetails.getFarePerUnit());
            this.travelTime.setText(DriverDetails.getTravelTime());
            this.fareApprox.setText(getResources().getString(R.string.currency_sign) + " " + DriverDetails.getFare());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
