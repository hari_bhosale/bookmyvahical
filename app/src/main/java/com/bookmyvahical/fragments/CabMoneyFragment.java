package com.bookmyvahical.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.bookmyvahical.R;
import com.bookmyvahical.MainActivity;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.helper.MyWebViewClient;

public class CabMoneyFragment extends BaseFragment {
    WebView cabMoneywebview;
    private Context context;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_cab_money, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.item_three));
        this.context = getActivity();
        try {
            this.cabMoneywebview = (WebView) root.findViewById(R.id.cabmoney_webview);
            MyWebViewClient.enableWebViewSettings(this.cabMoneywebview);
            this.cabMoneywebview.setWebViewClient(new MyWebViewClient(this.context));
            this.cabMoneywebview.loadUrl(ProjectURls.CAB_MONEY_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    protected void backPressed() {
        try {
            this.context.startActivity(new Intent(this.context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
