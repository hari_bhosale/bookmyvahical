package com.bookmyvahical.latlong;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

public class LatLongDetails {
    public static double destination_latitude = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    public static double destination_longitude = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    public static double driver_latitude = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    public static double driver_longitude = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    public static double user_latitude = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    public static double user_longitude = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
}
