package com.bookmyvahical.constants;

import com.bookmyvahical.R;

import java.util.ArrayList;

public class AppConstants {
    public static String[] CountryNamesString = new String[238];
    public static ArrayList<String> categories = new ArrayList();
    public static String[] catgories = new String[]{"Available", "Booked", "DND"};
    public static ArrayList<String> countryid = new ArrayList();
    public static ArrayList<String> countryname = new ArrayList();
    public static Integer[] imageids = new Integer[]{Integer.valueOf(R.drawable.available), Integer.valueOf(R.drawable.booked), Integer.valueOf(R.drawable.dnd), Integer.valueOf(R.drawable.dnd)};

    public static ArrayList<String> getcategories() {
        categories.clear();
        categories.add("Available");
        categories.add("Booked");
        categories.add("Pending");
        categories.add("DND");
        return categories;
    }
}
