package com.bookmyvahical.utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Base64;
import android.util.Log;
import java.io.ByteArrayOutputStream;

public class BitmapUtil {
    public static String encodeBitmapToBase64String(Bitmap bitmap) {
        byte[] ba;
        do {
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            Log.e("BEFORE REDUCING", bitmap.getHeight() + " " + bitmap.getWidth() + " " + (bitmap.getRowBytes() * bitmap.getHeight()));
            Log.e("After REDUCING", bitmap.getHeight() + " " + bitmap.getWidth() + " " + (bitmap.getRowBytes() * bitmap.getHeight()));
            bitmap.compress(CompressFormat.JPEG, 90, bao);
            ba = bao.toByteArray();
            if (ba.length / 1024 >= 650) {
                bitmap = Bitmap.createScaledBitmap(bitmap, (int) (((double) bitmap.getWidth()) * 0.95d), (int) (((double) bitmap.getHeight()) * 0.95d), true);
            }
            Log.e("BYTE LENGTH", "" + (ba.length / 1024));
        } while (ba.length / 1024 >= 650);
        return Base64.encodeToString(ba, 0);
    }
}
