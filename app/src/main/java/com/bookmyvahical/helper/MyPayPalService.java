package com.bookmyvahical.helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import java.math.BigDecimal;
import org.json.JSONException;
import org.json.JSONObject;

public class MyPayPalService {
    private static final String CONFIG_CLIENT_ID = "ARyK9vkN4BL4oYGkBp3LyVhp9ITAkcxpeqsrhEIXIEZRPZb6XKVRdJah1lTIzKwXR9vt7zv8RmMDRfR0";
    private static final String CONFIG_ENVIRONMENT = "live";
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    public static final int REQUEST_CODE_PAYMENT = 1;
    private static final String TAG = "paymentExample";
    public static PayPalConfiguration config = new PayPalConfiguration().environment("live").clientId(CONFIG_CLIENT_ID).merchantName("Reki Chandra").merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy")).merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    public static PayPalPayment getThingToBuy(String paymentIntent, String amount, String currency, String details) {
        return new PayPalPayment(new BigDecimal(amount), "USD", details, paymentIntent);
    }

    public static JSONObject processPayPalResponse(Context context, int requestCode, int resultCode, Intent data, WebView rides_webview) {
        JSONObject js = null;
        if (requestCode != 1) {
            return null;
        }
        if (resultCode == -1) {
            PaymentConfirmation confirm = (PaymentConfirmation) data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm == null) {
                return null;
            }
            try {
                Log.i("", confirm.toJSONObject().toString(4));
                js = confirm.toJSONObject();
                Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                Toast.makeText(context, "PaymentConfirmation info received from PayPal", 1).show();
                return js;
            } catch (JSONException e) {
                Toast.makeText(context, "an extremely unlikely failure occurred: ", 1).show();
                return js;
            }
        } else if (resultCode == 0) {
            Toast.makeText(context, "Payment has been canceled.", 0).show();
            return null;
        } else if (resultCode != 2) {
            return null;
        } else {
            Toast.makeText(context, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.", 1).show();
            return null;
        }
    }

    public static void startPayPalConfigurationsService(Context context) {
        Intent intent = new Intent(context, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        context.startService(intent);
    }
}
