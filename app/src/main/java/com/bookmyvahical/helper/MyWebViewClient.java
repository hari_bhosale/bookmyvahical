package com.bookmyvahical.helper;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.bookmyvahical.LoginActivity;
import com.bookmyvahical.SplashActivity;

public class MyWebViewClient extends WebViewClient {
    public Context mContext;
    private ProgressDialog mDialog;

    /* renamed from: com.bookmyvahical.helper.MyWebViewClient$1 */
    class C03781 implements OnClickListener {
        C03781() {
        }

        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            Intent intent = new Intent((Activity) MyWebViewClient.this.mContext, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            MyWebViewClient.this.mContext.startActivity(intent);
            Log.e("CHECK", "GOING TO LOGIN");
            ((Activity) MyWebViewClient.this.mContext).finish();
        }
    }

    public static class MyWebChromeClient extends WebChromeClient {
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            Log.d("WebViewDemo", message);
            result.confirm();
            return true;
        }
    }

    public MyWebViewClient(Context context) {
        this.mContext = context;
    }

    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        this.mDialog = ProgressDialog.show(this.mContext, "", "Loading...", true, true);
    }

    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        this.mDialog.dismiss();
    }

    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        super.onReceivedError(view, errorCode, description, failingUrl);
        Log.e("ERROR", "" + errorCode);
        Log.e("ERROR", "" + failingUrl);
        view.setVisibility(4);
        this.mDialog.dismiss();
        new Builder(this.mContext).setTitle("Connectivity Problem !!!!").setMessage("Please Check Your Internet Connection !!!!").setPositiveButton("OK", new C03781()).show();
    }

    public static void enableWebViewSettings(WebView web) {
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setSavePassword(false);
        web.getSettings().setSaveFormData(false);
        web.getSettings().setSupportZoom(false);
        web.getSettings().setAppCacheEnabled(true);
    }

    public static void handleIncomingIntent(Context context, Class<?> cls) {
        Intent intent = new Intent(context, SplashActivity.class);
        intent.setFlags(1140883456);
        intent.putExtra("EXIT", true);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    public static MyWebChromeClient getWebChromeInstance() {
        return new MyWebChromeClient();
    }
}
