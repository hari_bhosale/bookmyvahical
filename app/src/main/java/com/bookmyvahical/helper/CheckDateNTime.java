package com.bookmyvahical.helper;

import android.content.Context;
import android.text.format.Time;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;
import com.bookmyvahical.login.LoginDetails;
import com.bookmyvahical.model.UserData;

public class CheckDateNTime {
    public static UserData userData;
    Context context;

    public static int check(DatePicker datepicker, TimePicker timepicker, Context context) {
        userData = UserData.getinstance(context);
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        if (datepicker.getYear() < today.year) {
            Toast.makeText(context, "Wrong Year Selected", 1).show();
            return 1;
        } else if (datepicker.getYear() != today.year) {
            return 0;
        } else {
            if (datepicker.getMonth() < today.month) {
                Toast.makeText(context, "Wrong Month Selected", 1).show();
                return 1;
            } else if (datepicker.getMonth() != today.month) {
                return 0;
            } else {
                if (datepicker.getDayOfMonth() < today.monthDay) {
                    Toast.makeText(context, "Wrong Day Selected", 1).show();
                    return 1;
                } else if (datepicker.getDayOfMonth() != today.monthDay) {
                    return 0;
                } else {
                    if (timepicker.getCurrentHour().intValue() < today.hour) {
                        Toast.makeText(context, "Wrong Hour selected\t", 1).show();
                        return 1;
                    } else if (timepicker.getCurrentHour().intValue() != today.hour) {
                        return 0;
                    } else {
                        if (timepicker.getCurrentMinute().intValue() >= today.minute) {
                            return 0;
                        }
                        Toast.makeText(context, "Wrong Minutes", 1).show();
                        return 1;
                    }
                }
            }
        }
    }

    public static void make_time(TimePicker timePicker, DatePicker datePicker) {
        String AM_PM;
        Object obj;
        String MY_DATE_TIME = "";
        String MY_TIME_IN_FORMAT = "";
        String Month_String = "";
        int Hour = timePicker.getCurrentHour().intValue();
        int Minute = timePicker.getCurrentMinute().intValue();
        int Day = datePicker.getDayOfMonth();
        int Month = datePicker.getMonth();
        int Year = datePicker.getYear();
        userData.setUserTimehitformat("" + Hour + ":" + Minute);
        userData.setUserDatehitformat("" + Year + "-" + (Month + 1 < 10 ? "0" + (Month + 1) : Integer.valueOf(Month + 1)) + "-" + (Day < 10 ? "0" + Day : Integer.valueOf(Day)));
        Log.e("TIME", "" + Hour + " " + Minute + " " + Day + " " + Month + " " + Year);
        if (Hour < 12) {
            AM_PM = "AM";
        } else if (Hour == 12) {
            AM_PM = "Noon";
        } else if (Hour == 0) {
            AM_PM = "MidNight";
        } else {
            AM_PM = "PM";
        }
        if (Hour > 12) {
            Hour -= 12;
        }
        switch (Month + 1) {
            case 1:
                Month_String = "JAN";
                break;
            case 2:
                Month_String = "FEB";
                break;
            case 3:
                Month_String = "MAR";
                break;
            case 4:
                Month_String = "APR";
                break;
            case 5:
                Month_String = "MAY";
                break;
            case 6:
                Month_String = "JUN";
                break;
            case 7:
                Month_String = "JUL";
                break;
            case 8:
                Month_String = "AUG";
                break;
            case 9:
                Month_String = "SEP";
                break;
            case 10:
                Month_String = "OCT";
                break;
            case 11:
                Month_String = "NOV";
                break;
            case 12:
                Month_String = "DEC";
                break;
            default:
                Month_String = "";
                break;
        }
        MY_DATE_TIME = "" + (Hour < 10 ? "0" + Hour : Integer.valueOf(Hour)) + ":" + (Minute < 10 ? "0" + Minute : Integer.valueOf(Minute)) + " " + AM_PM + " on " + Day + " " + Month_String + " " + Year;
        StringBuilder append = new StringBuilder().append("").append(Hour < 10 ? "0" + Hour : Integer.valueOf(Hour)).append(":");
        if (Minute < 10) {
            obj = "0" + Minute;
        } else {
            obj = Integer.valueOf(Minute);
        }
        LoginDetails.UserTime = append.append(obj).append(" ").append(AM_PM).toString();
        LoginDetails.UserDate = "" + Day + " " + Month_String + " " + Year;
    }
}
