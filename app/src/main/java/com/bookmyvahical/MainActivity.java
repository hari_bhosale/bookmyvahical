package com.bookmyvahical;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bookmyvahical.fragments.CabMoneyFragment;
import com.bookmyvahical.fragments.DriverFragment;
import com.bookmyvahical.fragments.HelpFragment;
import com.bookmyvahical.fragments.HomeFragment;
import com.bookmyvahical.fragments.ProfileFragment;
import com.bookmyvahical.fragments.RateCardFragment;
import com.bookmyvahical.fragments.RideDetailsFragment;
import com.bookmyvahical.services.FcmTokenToServer;
import com.bookmyvahical.utils.SharedPreferencesUtility;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.drive.DriveFile;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnNavigationItemSelectedListener {
    private String base64String;
    private Context context;
    private NavigationView navigationView;
    private ImageView profilePic;
    private TextView userEmailId;
    private TextView userName;

    /* renamed from: com.bookmyvahical.MainActivity$1 */
    class C03521 implements OnClickListener {
        C03521() {
        }

        public void onClick(DialogInterface dialog, int id) {
            dialog.cancel();
        }
    }

    /* renamed from: com.bookmyvahical.MainActivity$2 */
    class C03532 implements OnClickListener {
        C03532() {
        }

        public void onClick(DialogInterface dialog, int id) {
            try {
                SharedPreferencesUtility.resetSharedPreferences(MainActivity.this.context);
                Intent intent = new Intent("android.intent.action.MAIN");
                intent.addCategory("android.intent.category.HOME");
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                MainActivity.this.startActivity(new Intent(MainActivity.this.context, LoginActivity.class).setFlags(67141632));
                MainActivity.this.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        try {
            this.context = this;
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            this.navigationView = (NavigationView) findViewById(R.id.nav_view);
            this.navigationView.setItemIconTintList(null);
            View headerView = this.navigationView.inflateHeaderView(R.layout.nav_header_main);
            this.userName = (TextView) headerView.findViewById(R.id.user_name);
            this.userEmailId = (TextView) headerView.findViewById(R.id.user_emailid);
            this.profilePic = (ImageView) headerView.findViewById(R.id.profile_picture);
            this.base64String = SharedPreferencesUtility.loadProfilePic(this.context);
            if (this.base64String.isEmpty()) {
                this.profilePic.setImageDrawable(getResources().getDrawable(R.drawable.user));
            } else {
                this.profilePic.setImageDrawable(getResources().getDrawable(R.drawable.user));
            }
            this.userName.setText(SharedPreferencesUtility.loadUsername(this.context));
            this.userEmailId.setText(SharedPreferencesUtility.loadUserType(this.context));
            int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.context);
            if (resultCode != 0) {
                if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                    Toast.makeText(this.context, R.string.playstore_not_install, 1).show();
                    GooglePlayServicesUtil.showErrorNotification(resultCode, this.context);
                } else {
                    Toast.makeText(this.context, R.string.playstore_not_support, 1).show();
                }
            }
            new FcmTokenToServer(this.context).execute(new String[0]);
            FragmentTransaction fragmentTransaction;
            if (SharedPreferencesUtility.loadUserType(this.context).equalsIgnoreCase("passenger")) {
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.containerView, new HomeFragment(), null);
                fragmentTransaction.commit();
            } else {
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.containerView, new DriverFragment(), null);
                fragmentTransaction.commit();
                this.navigationView.getMenu().getItem(4).setVisible(false);
                MenuItem item2 = this.navigationView.getMenu().getItem(3);
                item2.setTitle(getResources().getString(R.string.item_three));
                item2.setIcon(R.drawable.cabmoney);
            }
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();
            this.navigationView.setNavigationItemSelectedListener(this);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
    }

    public void onBackPressed() {
        try {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(8388611)) {
                drawer.closeDrawer(8388611);
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onResume() {
        super.onResume();
        Log.w("MainActivity", "onResume");
    }

    protected void onPause() {
        super.onPause();
        Log.w("MainActivity", "onPause");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        switch (item.getItemId()) {
            case R.id.home:
                if (!SharedPreferencesUtility.loadUserType(this.context).equalsIgnoreCase("passenger")) {
                    fragmentTransaction.replace(R.id.containerView, new DriverFragment(), null);
                    fragmentTransaction.commit();
                    break;
                }
                fragmentTransaction.replace(R.id.containerView, new HomeFragment(), null);
                fragmentTransaction.commit();
                break;
            case R.id.profile:
                fragmentTransaction.replace(R.id.containerView, new ProfileFragment(), null);
                fragmentTransaction.commit();
                break;
            case R.id.rides:
                fragmentTransaction.replace(R.id.containerView, new RideDetailsFragment(), null);
                fragmentTransaction.commit();
                break;
            case R.id.rate_card:
                if (!SharedPreferencesUtility.loadUserType(this.context).equalsIgnoreCase("passenger")) {
                    fragmentTransaction.replace(R.id.containerView, new CabMoneyFragment(), null);
                    fragmentTransaction.commit();
                    break;
                }
                fragmentTransaction.replace(R.id.containerView, new RateCardFragment(), null);
                fragmentTransaction.commit();
                break;
            case R.id.refer_friend:
                referFriendsMethod();
                break;
            case R.id.help:
                fragmentTransaction.replace(R.id.containerView, new HelpFragment(), null);
                fragmentTransaction.commit();
                break;
            case R.id.sign_out:
                signOutMethod();
                break;
        }
        ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(8388611);
        return true;
    }

    private void referFriendsMethod() {
        Resources resources = getResources();
        Intent emailIntent = new Intent();
        emailIntent.setAction("android.intent.action.SEND");
        emailIntent.putExtra("android.intent.extra.TEXT", "Hello");
        emailIntent.setType("message/rfc822");
        PackageManager pm = this.context.getPackageManager();
        Intent sendIntent = new Intent("android.intent.action.SEND");
        sendIntent.setType("text/plain");
        Intent openInChooser = Intent.createChooser(emailIntent, "Share");
        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
        List<LabeledIntent> intentList = new ArrayList();
        for (int i = 0; i < resInfo.size(); i++) {
            ResolveInfo ri = (ResolveInfo) resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            if (packageName.contains("android.email")) {
                emailIntent.setPackage(packageName);
            } else if (packageName.contains("twitter") || packageName.contains("facebook") || packageName.contains("mms") || packageName.contains("com.whatsapp") || packageName.contains("com.linkedin.android") || packageName.contains("com.viber.voip") || packageName.contains("com.skype.raider") || packageName.contains("com.google.android.talk") || packageName.contains("tencent.mm") || packageName.contains("com.tencent.mm.ui.tools.ShareToTimeLineUI") || packageName.contains("com.evernote") || packageName.contains("com.bsb.hike") || packageName.contains("com.google.android.apps.babel") || packageName.contains("android.gm")) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                intent.setAction("android.intent.action.SEND");
                intent.setType("text/plain");
                intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
            }
        }
        openInChooser.putExtra("android.intent.extra.INITIAL_INTENTS", (LabeledIntent[]) intentList.toArray(new LabeledIntent[intentList.size()]));
        startActivity(openInChooser);
    }

    private void signOutMethod() {
        Builder builder = new Builder(this);
        builder.setMessage((CharSequence) "Are you sure you want to signout?").setCancelable(false).setPositiveButton((CharSequence) "Yes", new C03532()).setNegativeButton((CharSequence) "No", new C03521());
        builder.create().show();
    }
}
