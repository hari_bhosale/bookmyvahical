package com.bookmyvahical;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.bookmyvahical.constants.ProjectURls;
import com.bookmyvahical.helper.MyWebViewClient;

public class ForgotPasswordActivity extends AppCompatActivity {
    private Context context;
    WebView forgot_WebView;
    private Handler mHandler = new Handler();

    final class DemoJavaScriptInterface {

        /* renamed from: com.bookmyvahical.ForgotPasswordActivity$DemoJavaScriptInterface$1 */
        class C03511 implements Runnable {
            C03511() {
            }

            public void run() {
                ForgotPasswordActivity.this.startActivity(new Intent(ForgotPasswordActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        }

        DemoJavaScriptInterface() {
        }

        @JavascriptInterface
        public void clickOnAndroidLogin() {
            ForgotPasswordActivity.this.mHandler.post(new C03511());
        }
    }

    final class MyWebChromeClient extends WebChromeClient {
        MyWebChromeClient() {
        }

        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            result.confirm();
            return true;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_forgot_password);
        try {
            this.context = this;
            this.forgot_WebView = (WebView) findViewById(R.id.forgot_password_webview);
            MyWebViewClient.enableWebViewSettings(this.forgot_WebView);
            this.forgot_WebView.setWebChromeClient(new MyWebChromeClient());
            this.forgot_WebView.setWebViewClient(new MyWebViewClient(this));
            this.forgot_WebView.addJavascriptInterface(new DemoJavaScriptInterface(), SystemMediaRouteProvider.PACKAGE_NAME);
            this.forgot_WebView.loadUrl(ProjectURls.FORGOT_PASSWORD_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
